package misc.leetCode.expressAlgoPrep;

import misc.leetCode.expressAlgoPrep.requiredDs.ListNode;

public class LinkedListMiddle {
    public ListNode middleNode(ListNode head) {
        int length = 0;
        ListNode currNode = head;
        while (currNode != null) {
            ++length;
            currNode = currNode.next;
        }
        int mid = length / 2;

        ListNode midNode = head;
        for (int i = 0; i < mid; i++) {
            midNode = midNode.next;
        }
        return midNode;
    }
}
