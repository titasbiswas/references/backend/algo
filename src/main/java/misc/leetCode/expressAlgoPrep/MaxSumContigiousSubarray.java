package misc.leetCode.expressAlgoPrep;

/**
 * Kadane's algorithm
 *
 * */
public class MaxSumContigiousSubarray {
    public static int maxSubArray(int[] nums) {
        int maxSum = nums[0];
        int currSum = nums[0];
        for (int i = 1; i < nums.length; i++){
            int curr = nums[i];
            currSum=Math.max(curr,currSum+curr);
            maxSum=Math.max(maxSum,currSum);
        }
        return maxSum;
    }

    public static void main(String[] args) {
        int[] nums = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        System.out.println(maxSubArray(nums));//[4, -1, 2, 1,] => 6
    }
}
