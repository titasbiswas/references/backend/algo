package misc.leetCode.expressAlgoPrep;

public class ReverseString {
    public static void reverseString(char[] s) {
        int len = s.length;
        for (int i = 0; i < len/2; i++) {
            char temp = s[i];
            s[i]=s[len-1-i];
            s[len-1-i] = temp;
        }
    }

    public static void main(String[] args) {
        char[] s = {'h','e','l','l','o'};
        reverseString(s);
        System.out.println(s);

        char[] s2 = {'H','a','n','n','a','h'};
        reverseString(s2);
        System.out.println(s2);
    }
}
