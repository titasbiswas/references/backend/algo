package misc.leetCode.expressAlgoPrep;

import java.util.Arrays;

public class MergeSortedArray {
    public static void merge1(int[] nums1, int m, int[] nums2, int n) {
        int p1 = m - 1, p2 = n - 1;
        for (int p = m + n - 1; p>=0; p--) {
            if(p2<0){
                break;
            }
            if(p1>=0 && nums1[p1]>nums2[p2]){
                nums1[p] = nums1[p1--];
            }else{
                nums1[p] = nums2[p2--];
            }
        }
    }

    public static void merge2(int[] nums1, int m, int[] nums2, int n) {
        int[] temp = Arrays.copyOfRange(nums1, 0, m);
        int i = 0, j = 0, k = 0;
        while (i < m && j < n) {
            if (temp[i] < nums2[j]) {
                nums1[k++] = temp[i++];
            } else {
                nums1[k++] = nums2[j++];
            }
        }

        while (i < m) {
            nums1[k++] = temp[i++];
        }

        while (j < n) {
            nums1[k++] = nums2[j++];
        }
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 2, 3, 0, 0, 0}, nums2 = {2, 5, 6};
        int m = 3, n = 3;
        merge1(nums1, m, nums2, n);
        System.out.println(Arrays.toString(nums1));
    }
}
