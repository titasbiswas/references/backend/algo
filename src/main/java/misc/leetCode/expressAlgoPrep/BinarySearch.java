package misc.leetCode.expressAlgoPrep;

public class BinarySearch {

    public static int binSearch(int[] arr, int low, int high, int target) {
        if (low > high) {
            return -1;
        }
        int mid = low + (high - low) / 2;
        if(arr[mid]==target){
            return mid;
        }
        else if(target<arr[mid]){
            return binSearch(arr, low, mid-1, target);
        }
        else {
            return binSearch(arr, mid+1, high, target);
        }
    }


    public static void main(String[] args) {
        int[] nums = {-1, 0, 3, 5, 9, 12};
        int target = 7; //Integer.MAX_VALUE;
        System.out.println(binSearch(nums, 0, nums.length-1, target));
    }
}
