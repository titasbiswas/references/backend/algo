package misc.leetCode.expressAlgoPrep;

import java.util.Arrays;

/***
 *Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.
 *Input: nums = [-4,-1,0,3,10]
 * Output: [0,1,9,16,100]
 * Explanation: After squaring, the array becomes [16,1,0,9,100].
 * After sorting, it becomes [0,1,9,16,100].
 */


public class SortedSquares {

    public static int[] sortedSquares(int[] nums) {
        int[] res = new int[nums.length];
        int left = 0;
        int right = nums.length - 1;
        int resP = res.length - 1;
        while (left <= right) {
            int ls = (int) Math.pow(nums[left], 2);
            int rs = (int) Math.pow(nums[right], 2);
            if (ls > rs) {
                res[resP--] = ls;
                ++left;
            } else {
                res[resP--] = rs;
                --right;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        int[] nums = {-4,-1,0,3,10};
        System.out.println(Arrays.toString(sortedSquares(nums)));
        System.out.println(3%7);
    }
}
