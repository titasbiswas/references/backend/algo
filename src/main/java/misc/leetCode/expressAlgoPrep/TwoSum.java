package misc.leetCode.expressAlgoPrep;

import java.util.Arrays;

/**
 *Given a 1-indexed array of integers numbers that is already sorted in non-decreasing order,
 * find two numbers such that they add up to a specific target number. Let these two numbers be numbers[index1] and numbers[index2]
 * where 1 <= first < second <= numbers.length.
 *
 * Return the indices of the two numbers, index1 and index2, as an integer array [index1, index2] of length 2.
 *
 * The tests are generated such that there is exactly one solution. You may not use the same element twice.
 * */
public class TwoSum {
    public static int[] twoSum(int[] numbers, int target) {
        int left = 0, right = numbers.length - 1;
        while(left < right) {
            if(numbers[left]+numbers[right]== target) {
                break;
            }else if(numbers[left]+numbers[right] > target){
                --right;
            }else {
                ++left;
            }
        }
        return new int[] {left+1, right+1};
    }

    public static void main(String[] args) {
        int[] numbers = {2,7,11,15};
        int target = 9;
        System.out.println(Arrays.toString(twoSum(numbers, target)));

        int[] numbers2 = {0,-1};
        int target2= -1;
        System.out.println(Arrays.toString(twoSum(numbers2, target2)));

        int[] numbers3 = {2, 3, 4};
        int target3= 6;
        System.out.println(Arrays.toString(twoSum(numbers3, target3)));
    }
}
