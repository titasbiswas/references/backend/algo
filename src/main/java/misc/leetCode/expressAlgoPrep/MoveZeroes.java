package misc.leetCode.expressAlgoPrep;

import java.util.Arrays;

/**
 * Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the non-zero elements.
 *
 * Don't worry about zeros, just fill them up with other numbers and then fill rest with zero
 * */
public class MoveZeroes {
    public static void moveZeroesToEnd(int[] nums) {
        //Two pointers, both starting from
        int write = 0;
        for (int i = 0; i < nums.length; i++){
            //If it is not a zero copy it to write position and increament write position
            if(nums[i] != 0){
                nums[write] = nums[i];
                ++write;
            }
        }

        //length-write counts all the zero, fill all the spaces with zero,
        while(write<nums.length){
            nums[write++] = 0;
        }
    }

    public static void moveZerosToFront(int[] nums){
        int len = nums.length;
        int read = len - 1, write = len-1;
        while(read>=0){
            if(nums[read] != 0){
                nums[write--] = nums[read];
            }
            --read;
        }

        while(write>=0){
            nums[write--] = 0;
        }
    }

    public static void main(String[] args) {
        int[] nums = {0,1,0,3,12};
        moveZeroesToEnd(nums);
        System.out.println(Arrays.toString(nums));

        int[] nums2 = new int[]{1, 10, 20, 0, 59, 63, 0, 88, 0};
        moveZerosToFront(nums2);
        System.out.println(Arrays.toString(nums2));
    }
}
