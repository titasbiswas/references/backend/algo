package misc.leetCode.expressAlgoPrep;

/**
 * Suppose you have n versions [1, 2, ..., n] and you want to find out the first bad one, which causes all the following ones to be bad.
 *
 * How about the terminating condition? We could guess that left and right eventually both meet and it must be the first bad version
 * */
//How about the terminating condition? We could guess that left and right eventually both meet and it must be the first bad version
public class FirstBadVersion {
    static int badVersion = 43;

    public static int firstBadVersion(int n) {
        int left = 1, right = n;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (isBadVersion(mid)) { // say 45, so first bad present in left
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left;//returns the lowest one
    }

    public static boolean isBadVersion(int version) {
        return version >= FirstBadVersion.badVersion ? true : false;
    }

    public static void main(String[] args) {

        System.out.println(firstBadVersion(88));
    }
}
