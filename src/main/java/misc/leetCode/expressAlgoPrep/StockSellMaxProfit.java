package misc.leetCode.expressAlgoPrep;


public class StockSellMaxProfit {
    public static int maxProfit(int[] arr) {
        int minTillNow = Integer.MAX_VALUE;
        int maxProfit = 0;
        for (int i = 0; i < arr.length; i++) {
            minTillNow = Math.min(minTillNow, arr[i]);
            maxProfit = Math.max(maxProfit, arr[i] - minTillNow);
        }
        return maxProfit;
    }

    public static void main(String[] args) {
        int[] arr = {10, 2, 5, 7, 8, 3, 4, 5};
        System.out.println(maxProfit(arr));//6

        int[] arr2 = {10, 9, 8, 8, 5, 4, 3};
        System.out.println(maxProfit(arr2));//0

        int[] arr3 = {0, 0, 0, 1};
        System.out.println(maxProfit(arr3));//1
    }
}
