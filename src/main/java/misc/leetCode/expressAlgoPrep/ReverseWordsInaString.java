package misc.leetCode.expressAlgoPrep;
/**
 * Given a string s, reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.
 * Input: s = "God Ding"
 * Output: "doG gniD"
 *
 * Use temp word to store and end when ' ' found
 *
 * */
public class ReverseWordsInaString {
    public static String reverseWords(String s) {
        final StringBuilder res  = new StringBuilder();
        final StringBuilder temp  = new StringBuilder();

        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) != ' ') {
                temp.append(s.charAt(i));
            }else{
                temp.reverse().append(" ");
                res.append(temp);
                temp.setLength(0);
            }
        }
        //for the last word
        res.append(temp.reverse());
        return res.toString();
    }

    public static void main(String[] args) {
        String s = "God Ding";
        System.out.println(reverseWords(s));
    }
}
