package misc.leetCode.expressAlgoPrep;

import java.util.HashMap;
import java.util.Map;

public class ContainsPermutation {
    public static boolean checkInclusion(String s1, String s2) {
        int k = s1.length();
        for (int i = k; i <= s2.length(); i++) {
            String currSubString = s2.substring(i - k, i);
            if (containsPermutation(currSubString, s1)) {
                return true;
            }
        }
        return false;
    }

    private static boolean containsPermutation(String currSubString, String s1) {
        int k = s1.length();
        Map<Character, Integer> countMap = new HashMap<>();
        for (int i = 0; i < k; i++) {
            char c = s1.charAt(i);
            countMap.merge(c, 1, Integer::sum);
        }
        for (int i = 0; i < currSubString.length(); i++) {
            char c = currSubString.charAt(i);
            countMap.computeIfPresent(c, (key, v) -> v == 1 ? null : v - 1);
        }
        if (countMap.isEmpty())
            return true;
        return false;
    }

    public static void main(String[] args) {
       String s1 = "ab", s2 = "eidbaooo";
        System.out.println(checkInclusion(s1, s2));
    }
}
