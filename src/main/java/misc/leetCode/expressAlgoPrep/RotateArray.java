package misc.leetCode.expressAlgoPrep;

import java.util.Arrays;

/**
 * Given an array, rotate the array to the right by k steps, where k is non-negative.
 * Input: nums = [1,2,3,4,5,6,7], k = 3
 * Output: [5,6,7,1,2,3,4]
 * Explanation:
 * rotate 1 steps to the right: [7,1,2,3,4,5,6]
 * rotate 2 steps to the right: [6,7,1,2,3,4,5]
 * rotate 3 steps to the right: [5,6,7,1,2,3,4]
 */
public class RotateArray {

    //Rotate using reverse
    //This approach is based on the fact that when we rotate the array k times, k elements from the back end of the array come to the front
    // and the rest of the elements from the front shift backwards.
    //In this approach, we firstly reverse all the elements of the array.
    // Then, reversing the first k elements followed by reversing the rest n-kn−k elements gives us the required result.

    private static void rorateUsingReverse(int[] nums, int k) {
        int len = nums.length;
        k = k % len;
        reverse(nums, 0, len-1);
        reverse(nums, 0, k-1);
        reverse(nums, k, len-1);
    }

    private static void reverse(int[] arr, int start, int end){
        while(start < end){
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            ++start;
            --end;
        }
    }


    //Copy the array elements [i] to [(i+k)%array.length] ----- (0+3)%7=3, 1 is going to index 3, i.e 4th position;
    public static void rotate1(int[] nums, int k) {
        int len = nums.length;
        int[] res = new int[len];
        for (int i = 0; i < len; i++) {
            res[(i + k) % len] = nums[i];
        }

        for (int i = 0; i < len; i++) {
            nums[i] = res[i];
        }
    }

    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5, 6, 7};
        int k = 3;
        rotate1(nums, k);
        System.out.println(Arrays.toString(nums));

        int[] nums1 = {1, 2, 3, 4, 5, 6, 7};
        int k1 = 3;
        rorateUsingReverse(nums1, k1);
        System.out.println(Arrays.toString(nums1));
    }
}
