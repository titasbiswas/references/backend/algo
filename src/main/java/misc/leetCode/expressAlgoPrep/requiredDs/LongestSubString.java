package misc.leetCode.expressAlgoPrep.requiredDs;

import java.util.HashMap;
import java.util.Map;

/**
 * if s[j]s[j] have a duplicate in the range [i, j)[i,j) with index j'j
 * ′
 * , we don't need to increase ii little by little. We can skip all the elements in the range [i, j'][i,j
 * ′
 * ] and let ii to be j' + 1j
 * ′
 * +1 directly.
 */
public class LongestSubString {

    public static int lengthOfLongestSubstring(String s) {
        String outputStr = ""; //The substring is itself not required but it's an addition to the problem
        Map<Character, Integer> indexmap = new HashMap<Character, Integer>();

        for (int start = 0, end = 0; end < s.length(); end++) {
            char currChar = s.charAt(end);
            if (indexmap.containsKey(currChar)) {
                start = Math.max(indexmap.get(currChar) + 1, start);
            }
            //Compare with current output
            if (outputStr.length() < end - start + 1) {
                outputStr = s.substring(start, end + 1);
            }
            indexmap.put(currChar, end);
        }
        return outputStr.length();
    }

    public static void main(String[] args) {
        String s = "pwwkew";
        System.out.println(lengthOfLongestSubstring(s));
    }
}
