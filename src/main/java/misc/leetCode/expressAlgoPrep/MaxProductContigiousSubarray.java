package misc.leetCode.expressAlgoPrep;

/**
 * based on three numbers: current number, product of mintillnow and current number, product of maxtillnow and current number
 * */
public class MaxProductContigiousSubarray {
    public static int  maxProduct(int[] arr){
        int res = 0;

        int minTillNow = arr[0];
        int maxTillNow = arr[0];

        for(int i = 1; i < arr.length; i++){
            int curr = arr[i];

            int temp_max = Math.max(curr, Math.max(minTillNow*curr, maxTillNow*curr));
            minTillNow = Math.min(curr, Math.min(minTillNow*curr, maxTillNow*curr));

            maxTillNow = temp_max;
            res = Math.max(res, maxTillNow);
        }
        return res;
    }

    public static void main(String[] args) {
        int[] arr = {2, -5, 4, -6, 0, 2, -10, 5};
        System.out.println(maxProduct(arr));
    }
}
