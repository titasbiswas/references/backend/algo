package misc.leetCode.ggl.StringArray;


public class RotateMatrix_6 {

    public static void rotate(int[][] matrix) {

        transpose(matrix);
        reflectH(matrix);

    }

    private static void transpose(int[][] matrix) {

        for (int i = 0; i < matrix.length; i++) {
            for (int j = i; j < matrix.length; j++){
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
    }

    private static void reflectH(int[][] matrix) {
        int n = matrix.length;
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n/2; j++){
                int temp = matrix[i][j];
                matrix[i][j] = matrix[i][n-1-j];
                matrix[i][n-1-j] = temp;
            }
        }
    }

    public static void main(String[] args) {
        int [][] matrix = {{5,1,9,11},{2,4,8,10},{13,3,6,7},{15,14,12,16}};
        printMatrix(matrix);
        rotate(matrix);
        printMatrix(matrix);
    }
    private static void printMatrix(int[][] matrix){
        for (int[] row: matrix){
            System.out.print("[");
            for(int cell: row){
                System.out.print(cell+",");
            }
            System.out.print("]\n");
        }
    }
}
