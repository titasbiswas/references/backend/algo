package misc.leetCode.ggl.StringArray;

import java.util.Arrays;

public class PlusOne_8 {

    public static int[] plusOne(int[] digits) {
        int len = digits.length;
        for (int i = len - 1; i >= 0; i--) {
            if (digits[i] == 9) {
                digits[i] = 0;
            } else {
                digits[i]++;
                return digits;
            }
        }
        //all are 9, so we are here, not returned from the earlier return
        digits = new int[len + 1];
        digits[0] = 1;
        return digits;
    }

    public static void main(String[] args) {
        int[] digits = {9, 9, 9};
        Arrays.stream(plusOne(digits)).forEach(System.out::print);

    }
}
