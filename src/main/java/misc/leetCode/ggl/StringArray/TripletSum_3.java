package misc.leetCode.ggl.StringArray;

import java.util.*;

public class TripletSum_3 {


    public static List<List<Integer>> threeSum1(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> res = new ArrayList<>();
        int calSum = 0;
        int len = nums.length;

        for (int i = 0; i < len && nums[i] <= 0; i++) {
            if (i == 0 || nums[i] != nums[i - 1]) {
                checkSum(nums, len, i, res);
            }
        }
        return res;
    }

    private static void checkSum(int[] nums, int len, int i, List<List<Integer>> res) {
        int left = i + 1;
        int right = len - 1;
        while (left < right) {
            int sum = nums[i] + nums[left] + nums[right];
            if (sum < 0) {
                ++left;
            } else if (sum > 0) {
                --right;
            } else {
                List tempList = new ArrayList<>();
                tempList.add(nums[i]);
                tempList.add(nums[left++]);
                tempList.add(nums[right--]);
                res.add(tempList);
                while (left < right && nums[left - 1] == nums[left]) {//skip duplicates
                    ++left;
                }
            }
        }

    }

    public static void main(String[] args) {
        int[] nums = {-1, 0, 1, 2, -1, -4};
        System.out.println(threeSum1(nums));
    }
}
