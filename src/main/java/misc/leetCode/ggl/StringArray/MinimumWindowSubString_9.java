package misc.leetCode.ggl.StringArray;

import java.util.HashMap;
import java.util.Map;

public class MinimumWindowSubString_9 {
    public static String minWindow(String s, String t) {
        String res = "";
        if(t.length() ==0 || s.length() ==0){
            return res;
        }
        if (s.length() < t.length()) {
            return res;
        }
        Map<Character, Integer> tMap = new HashMap<>();
        for (int i = 0; i < t.length(); i++) {
            Character temp = t.charAt(i);
            tMap.merge(temp, 1, Integer::sum);
        }
        int left = 0, right = 0;
        //int[] v = {-1, 0, 0};
        while (right < s.length()) {

            while(contains(s.substring(left, right+1), tMap) && left<=right){
                String temp = s.substring(left, right+1);
                if(res==""){
                    res = temp;
                }
                res = temp.length() < res.length()?temp:res;
                ++left;
            }
            ++right;

        }
        return res;
    }

    private static boolean contains(String container, Map<Character, Integer> tMap) {
        Map<Character, Integer> cMap = new HashMap<>();
        for (Character c : container.toCharArray()) {
            cMap.merge(c, 1, Integer::sum);
        }
        for(Map.Entry<Character, Integer> e: tMap.entrySet()){
            if(cMap.getOrDefault(e.getKey(), 0)<e.getValue()){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        String S = "ADOBECODEBANC";
        String T = "ABC";
        System.out.println(minWindow(S, T));
    }
}
