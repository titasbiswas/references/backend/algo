package misc.leetCode.ggl.StringArray;

import java.util.HashMap;
import java.util.Map;

public class LongestSubstringTwoDistinct_11 {

    public static int lengthOfLongestSubstringTwoDistinct(String s) {
        int len = s.length();
        int left = 0, right = 0, res = 0;

        while (left <= right) {
            while (checkTwoDistinct(s.substring(left, right)) &&  right < len) {
                res = Math.max(res, right - left);
                ++right;
            }
            ++left;
        }
        return res;
    }

    private static boolean checkTwoDistinct(String substring) {
        Map<Character, Integer> cMap = new HashMap<>();
        for (Character c : substring.toCharArray()) {
            int count = cMap.getOrDefault(c, 0);
            cMap.put(c, ++count);
            if (cMap.size()>2) {
                return false;
            }

        }
        return true;
    }

    public static void main(String[] args) {
        String s = "eceba";
        System.out.println(lengthOfLongestSubstringTwoDistinct(s));
    }
}
