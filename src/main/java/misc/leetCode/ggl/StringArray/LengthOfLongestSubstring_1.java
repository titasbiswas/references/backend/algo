package misc.leetCode.ggl.StringArray;

import java.util.HashSet;
import java.util.Set;

public class LengthOfLongestSubstring_1 {

    public static void main(String[] args) {
        String str = "abcabcab";
        System.out.println(lengthOfLongestSubstring2(str));
    }

    public static int lengthOfLongestSubstring1(String s) {
        int len = s.length();
        int maxLen = 0;
        for (int i = 0; i < len; i++) {
            for (int j = i; j < len; j++){
                if(checkUnique(s, i, j)){
                    maxLen = Math.max(maxLen, j-i+1);
                }
            }
        }
        return maxLen;
    }

    public static int lengthOfLongestSubstring2(String s) {
        int len = s.length();
        int left = 0;
        int right = 0;
        int maxLen = 0;
        int[] chars = new int[128]; //O(m), charset of the string
        while(right<len){
            char r = s.charAt(right);
            chars[r]++;
            while(chars[r]>1){
                char l = s.charAt(left);
                chars[l]--;
                left++;
            }
            maxLen = Math.max(maxLen, right-left+1);
            right++;
        }
        return maxLen;
    }

    private static boolean checkUnique(String s, int start, int end) {
        Set<Character> chars = new HashSet<>();
        for (int i=start; i<=end;i++){
            char c = s.charAt(i);
            if(chars.contains(c))
                return false;
            chars.add(c);
        }
        return true;
    }
}
