package misc.leetCode.ggl.StringArray;

import java.util.Arrays;

public class NextGreaterPermutation_4 {
    public static void nextPermutation(int[] nums) {
        int i = nums.length - 2;
        //decrement till nums[i+1] not less than nums[i]
        // Get the first digit where it is smaller that previous digit
        while (i >= 0 && nums[i + 1] <= nums[i]) {
            --i;
        }
        //We know that all digits are linearly sorted in reverse order except one digit which was swapped.
        //As they will be in decreasing order so to find the smallest element possible from the right part we just reverse them thus reducing time complexity.
        if (i >= 0) {
            int j = nums.length-1;
            while (nums[j] <= nums[i]) {
                --j;
            }
            swap(nums, i, j);
        }
        //reverse from i+1 till end
        reverse(nums, i + 1);
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private static void reverse(int[] arr, int start) {
        int left = start, right = arr.length - 1;
        while (left < right) {
            swap(arr, left, right);
            ++left;
            --right;
        }
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3};
        nextPermutation(nums);
        Arrays.stream(nums).forEach(System.out::print);
    }
}
