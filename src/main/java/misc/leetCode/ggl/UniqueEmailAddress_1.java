package misc.leetCode.ggl;


import java.util.Arrays;
import java.util.stream.Collectors;

public class UniqueEmailAddress_1 {

    public static int numUniqueEmails(String[] emails) {

        if(emails == null || emails.length == 0)
            return 0;

        return Arrays.stream(emails).parallel().map(e -> {
                    String[] tempArr = e.split("@");
                    String lName = tempArr[0], dName = tempArr[1];
                    if (lName.contains("+")) {
                        lName = lName.split("\\+")[0];
                    }
                    if (lName.contains(".")) {
                        String[] lArr = lName.split("\\.");
                        lName = String.join("", lArr);
                    }
                    return lName + "@" + dName;
                })
                .collect(Collectors.toSet()).size();

    }

    public static void main(String[] args) {
        String[] test1  = {"test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"};
        System.out.println(numUniqueEmails(test1));

        String[] test2 = {"a@leetcode.com","b@leetcode.com","c@leetcode.com"};
        System.out.println(numUniqueEmails(test2));


        String[] test3 = {"test.emailalex@leetcode.com","test.e.mailbob.cathy@leetcode.com","testemail+david@lee.tcode.com"};
        System.out.println(numUniqueEmails(test3));

    }
}
