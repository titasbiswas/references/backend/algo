package misc.leetCode.ggl.LinkedList;

public class AddTwoNumbers {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode resHead = new ListNode();
        ListNode p1 = l1, p2 = l2, curr = resHead;
        int carry = 0;
        while (p1 != null || p2 != null) {
            int x = p1 != null ? p1.val : 0;
            int y = p2 != null ? p2.val : 0;

            int sum = x + y + carry;
            carry = sum / 10;
            sum = sum % 10;
            curr.next = new ListNode(sum);
            curr = curr.next;
            if (p1 != null)
                p1 = p1.next;
            if (p1 != null)
                p2 = p2.next;
        }
        if (carry > 0) {
            curr.next = new ListNode(carry);
        }

        return resHead.next;
    }
}
