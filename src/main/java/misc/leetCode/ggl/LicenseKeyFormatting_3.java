package misc.leetCode.ggl;

import java.util.Deque;
import java.util.LinkedList;

public class LicenseKeyFormatting_3 {

    public static String licenseKeyFormatting(String s, int k) {
        int i = s.length() - 1, c = 0;
        Deque<String> q = new LinkedList<>();
        for (; i >= 0; i--) {
            if (s.charAt(i) == '-') {
                continue;
            }

            q.push(Character.toString(s.charAt(i)).toUpperCase());
            c++;

            if (c == k && i != 0) {
                q.push("-");
                c = 0;

            }
        }
        if(!q.isEmpty() && q.peekFirst().equals("-")){
            q.removeFirst();
        }
        return String.join("", q);
    }

    public static void main(String[] args) {
        System.out.println(licenseKeyFormatting("5F3Z-2e-9-w", 4));
        System.out.println(licenseKeyFormatting("--a-a-a-a--", 2));
    }
}
