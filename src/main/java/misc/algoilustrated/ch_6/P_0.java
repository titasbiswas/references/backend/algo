package misc.algoilustrated.ch_6;

import java.util.Arrays;

public class P_0 {
    public static void main(String[] args) {

        int[] arr = new int[]{2, 3, 5, 7, 8, 10, 20, 6, 9, 34, 22, 1};
        solution(arr);
        System.out.println(Arrays.toString(arr));
    }

    //Array even odd segregation
    static void solution(int... arr){
        int nextEven = 0, nextOdd = arr.length-1;
        while(nextEven < nextOdd){
            if(arr[nextEven]%2 == 0)
                nextEven++;
            else {
                int temp = arr[nextEven];
                arr[nextEven] = arr[nextOdd];
                arr[nextOdd--] = temp;
            }
        }
    }

}
