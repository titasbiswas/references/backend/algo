package misc.epi.prt_1.lrn_0;


import java.math.BigInteger;

public class Karatsuba {

    public static void main(String[] args) {

        BigInteger result = multK(new BigInteger("3141592653589793238462643383279502884197169399375105820974944592"),
                new BigInteger("2718281828459045235360287471352662497757247093699959574966967627"));
        System.out.println(result);
    }


    /**
     * Recursive method for multiplication
     * Assumptions: x and y both have same (n)umber of digits and
     * n is a power of two
     * The key is (a+b)*(c+d) - (a*c) - (b*d) = a*d + b*c.
     */
    private static BigInteger multK(BigInteger x, BigInteger y) {
        if (x.divide(BigInteger.TEN).equals(BigInteger.ZERO)) {
            return x.multiply(y);
        } else {
            int n = deduceLen(x);
            BigInteger a = x.divide(BigInteger.valueOf((long) Math.pow(10, n / 2)));
            BigInteger b = x.mod((BigInteger.valueOf((long) Math.pow(10, n / 2))));

            BigInteger c = y.divide((BigInteger.valueOf((long) Math.pow(10, n / 2))));
            BigInteger d = y.mod((BigInteger.valueOf((long) Math.pow(10, n / 2))));

            BigInteger p = a.add(b);
            BigInteger q = c.add(b);

            BigInteger ac = multK(a, c), bd = multK(b, d), pq = multK(p, q);
            return
                    ac.multiply(BigInteger.valueOf((long) Math.pow(10, n)))
                            .add(BigInteger.valueOf((long) Math.pow(10, n)).multiply(pq.subtract(ac).subtract(bd)))
                            .add(bd);
        }
    }

    private static int deduceLen(BigInteger number) {

        return number.toString().length();
    }
}
