package misc.epi.prt_1.lrn_0;


public class RecIntMult {

    public static void main(String[] args) {

        int result = mult1(1234, 5678);
        System.out.println(result);
    }


    /**
     * Recursive method for multiplication
     * Assumptions: x and y both have same (n)umber of digits and
     *  n is a power of two
     */
    private static int mult1(int x, int y) {
        if (x / 10 == 0) {
            return x * y;
        } else {
            int n = deduceLen(x);
            int a = (int) (x / Math.pow(10, n / 2));
            int b = (int) (x % Math.pow(10, n / 2));

            int c = (int) (y / Math.pow(10, n / 2));
            int d = (int) (y % Math.pow(10, n / 2));

            int ac = mult1(a, c), ad = mult1(a, d), bc = mult1(b, c), bd = mult1(b, d);
            return (int) (
                    (Math.pow(10, n) * ac)
                            + (Math.pow(10, n / 2) * (ad + bc))
                            + bd);
        }
    }

    private static int deduceLen(int number) {
        int length = 0;
        long temp = 1;
        while (temp <= number) {
            length++;
            temp *= 10;
        }
        return length;
    }
}
