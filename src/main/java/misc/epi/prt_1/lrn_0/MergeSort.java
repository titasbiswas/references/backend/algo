package misc.epi.prt_1.lrn_0;

import java.util.Arrays;

public class MergeSort {
    public static void main(String[] args) {

        int[] arr = {-9, -1, -5, -3, -6, -7, 8, -2, -4};
        Arrays.stream(sort(arr, arr.length)).mapToObj(e -> e+" ").forEach(System.out::print);
    }

    public static int[] sort(int[] arr, int len) {
        if (len <= 1)
            return arr;
        int lenA;
        if (len % 2 == 0)
            lenA = len / 2;
        else
            lenA = (len / 2) + 1;

        int[] arrA = sort(Arrays.copyOfRange(arr, 0, lenA), lenA);
        int[] arrB = sort(Arrays.copyOfRange(arr, lenA, len), len - lenA);
        return merge(arrA, arrB);

    }

    public static int[] merge(int[] arrA, int[] arrB) {

        int i = 0, j = 0, k = 0, lenA = arrA.length, lenB = arrB.length;
        int len = lenA + lenB;
        int[] res = new int[len];
        while (j < lenA && k < lenB) {
            if (arrA[j] < arrB[k])
                res[i] = arrA[j++];
            else
                res[i] = arrB[k++];
            i++;
        }

        while (j < lenA) {
            res[i++] = arrA[j++];
        }

        while (k < lenB)
            res[i++] = arrB[k++];

        return res;
    }
}

