package misc.playground.lru;

import java.util.LinkedHashSet;
import java.util.Set;

public class LRUCache_LinkedHashSet {

    private Set<Integer> cache;
    private final int CACHE_CAPACITY;
    public LRUCache_LinkedHashSet(int capacity){
        CACHE_CAPACITY = capacity;
        cache = new LinkedHashSet<>();
    }

    public int refer(int key){
        if(cache.contains(key)){
            cache.remove(key);
        }
        else{
            if(CACHE_CAPACITY == cache.size()){
                int tempKey = cache.iterator().next();
                cache.remove(tempKey);
            }
        }
        cache.add(key);
        return key;
    }

    public void display(){
        cache.forEach(System.out::println);
    }

    public static void main(String[] args) {
        LRUCache_LinkedHashSet ca = new LRUCache_LinkedHashSet(4);
        ca.refer(1);
        ca.refer(2);
        ca.refer(3);
        ca.refer(1);
        ca.refer(4);
        ca.refer(5);
        ca.display();
    }
}
