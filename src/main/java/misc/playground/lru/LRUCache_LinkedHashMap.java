package misc.playground.lru;

import java.util.LinkedHashMap;
import java.util.Map;

public class LRUCache_LinkedHashMap {

    private Map<Integer, String> cache;

    public LRUCache_LinkedHashMap(int capacity){

        cache = new LinkedHashMap<Integer, String>(){
            @Override
            protected boolean removeEldestEntry(Map.Entry eldest) {
                System.out.println("Removing..."+ eldest);
                return size() > capacity;
            }
        };
    }

    public String get(int key){
        return cache.getOrDefault(key, null);
    }

    public void put(int key, String value){
        cache.put(key, value);
    }

    public void display(){
        System.out.println(cache);
    }


    public static void main(String[] args) {
        LRUCache_LinkedHashMap cache = new LRUCache_LinkedHashMap(5);
        cache.put(1, "apan");
        cache.put(2, "bapan");
        cache.put(3, "chowki");
        cache.put(4, "chapan");
        cache.put(5, "ke");
        cache.put(2, "kheyeche");
        cache.put(1, "kar");
        cache.put(7, "thapan");

        cache.display();






    }
}
