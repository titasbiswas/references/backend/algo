package misc.playground.lru;

import java.util.HashMap;

public class LRUCache {
    Node head;
    Node tail;
    HashMap<Integer, Node> map = null;
    int cap = 0;

    public LRUCache(int capacity) {
        this.cap = capacity;
        map = new HashMap<>(capacity);
    }

    public int get(int key) {
        if (map.get(key) == null) {
            return -1;
        }

        Node node = map.get(key);

        //if node is tail, no need to remove and offer
        if (node.key != tail.key) {
            removeNode(node);
            offerNode(node);
        }

        return node.value;
    }

    public void put(int key, int value) {
        if (map.containsKey(key)) {
            Node node = map.get(key);
            node.value = value;

            removeNode(node);
            offerNode(node);
        } else {
            if (map.size() >= cap) {
                map.remove(head.key);
                removeNode(head);
            }

            Node newNode = new Node(key, value);
            offerNode(newNode);
            map.put(newNode.key, newNode);
        }
    }

    private void removeNode(Node node) {
        if (node.prev != null) {
            node.prev.next = node.next;
        } else {
            head = node.next;
        }

        if (node.next != null) {
            node.next.prev = node.prev;
        } else {
            tail = node.prev;
        }
    }

    private void offerNode(Node node) {
        if (tail != null) {
            tail.next = node;
        }

        node.prev = tail;
        node.next = null;
        tail = node;

        if (head == null) {
            head = tail;
        }
    }

}

class Node {
    int key;
    int value;
    Node prev;
    Node next;

    public Node(int key, int value) {
        this.key = key;
        this.value = value;
    }
}
