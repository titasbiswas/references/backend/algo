package misc.playground.hkrrnk;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

//Make anagram by deleting lowest number of character
public class MakingAnagram {

    public static void main(String[] args) {

        System.out.println(makeAnagram("jxwtrhvujlmrpdoqbisbwhmgpmeoke", "fcrxzwscanmligyxyvym"));
    }

    static int makeAnagram(String a, String b) {
        AtomicInteger c= new AtomicInteger(0);
        Map<Character, Integer> ma = new HashMap<>();
        Map<Character, Integer> mb = new HashMap<>();
        a.chars().mapToObj(i->(char)i).forEach(ch ->{
            if(ma.containsKey(ch)){
                ma.put(ch, ma.get(ch)+1);
            }
            else{
                ma.put(ch, 1);
            }
        });

      // ma = a.chars().mapToObj(c -> (char)c).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        b.chars().mapToObj(i->(char)i).forEach(ch ->{
            if(mb.containsKey(ch)){
                mb.put(ch, mb.get(ch)+1);
            }
            else{
                mb.put(ch, 1);
            }
        });

       // mb = b.chars().mapToObj(c -> (char)c).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));


        ma.keySet().forEach(k ->{
            if(mb.containsKey(k)){
                c.getAndAdd(Math.abs(mb.get(k)-ma.get(k)));
                mb.remove(k);
            }
            else{
                c.getAndAdd(ma.get(k));
            }
        });

        mb.values().forEach(v ->{
            c.getAndAdd(v);
        });
        return c.get();
    }
}
