package misc.playground.common;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public class FileOperations {
    private static final String INPUT_FILE = "src/main/resources/input.txt";
    private static final String OUTPUT_FILE = "src/main/resources/output.txt";

    public static void main(String[] args) throws Exception {

        Function<String, RespData> processor = in -> {
            RespData data = new RespData();
            String[] tokens = in.split(" ");
            data.setOrigin(tokens[0]);

            String dtString = tokens[3].replace("[", "");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MMM/yyyy:hh:mm:ss"); //01/Jul/1995:00:00:12
            //LocalDateTime dateTime = LocalDateTime.parse(dtString, formatter);
            //data.setDate(dateTime);

            String reqStr = in.substring(in.indexOf("\"") + 1, in.lastIndexOf("\""));
            String[] reqTokens = reqStr.split(" ");
            data.setMethod(reqTokens[0]);
            String path = reqTokens[1];
            data.setFileName(Optional.of(path.substring(path.lastIndexOf("/")+1)));

            String[] endTokens = in.substring(in.lastIndexOf("\"") + 1).split(" ");
            data.setRespCode(Integer.valueOf(endTokens[1]));
            data.setSize(Long.valueOf(endTokens[2]));

            return data;
        };




        Path outFilePath = Paths.get(OUTPUT_FILE).toAbsolutePath();
        Stream<String> inFileStream = Files.lines(Paths.get(INPUT_FILE).toAbsolutePath());
        inFileStream
                .filter(line -> line.contains(".GIF") || line.contains(".gif"))
                .map(processor)
                .filter(e -> e.getRespCode() == 200)
                .forEach(o -> {
                    String line = o.getFileName().get() + "\n";
                    try {
                        Files.write(outFilePath, line.getBytes(), StandardOpenOption.APPEND);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        inFileStream.close();
    }


}

@lombok.Data
class RespData {
    private String origin;
    private LocalDateTime date;
    private String method;
    private String path;
    private Optional<String> fileName;
    private int respCode;
    private long size;
}
