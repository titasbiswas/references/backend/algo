package misc.playground.gfg;

import java.util.Arrays;

public class TripletSum_Array {

    public static void main(String args[])
    {
        int arr[] = { 5, 32, 1, 7, 10, 50, 19, 21, 2 };
        int n = arr.length;
        findTriplet(arr, n);
    }

    private static void findTriplet(int[] arr, int n) {

        int j=0, k=0;

        Arrays.sort(arr);

        for (int i=n-1; i>0;i--){
            j=0;
            k=i-1;
            while(k>j){
                if (arr[j]+arr[k]==arr[i]) {
                    System.out.printf("Find one triplet : %d %d %d \n", arr[i], arr[j], arr[k]);
                    break;
                }
                else if(arr[k]+arr[j]>arr[i]){
                    k--;
                }
                else{
                    j++;
                }
            }
        }
    }
}
