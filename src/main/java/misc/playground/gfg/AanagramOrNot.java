package misc.playground.gfg;

import java.util.HashMap;
import java.util.Map;

public class AanagramOrNot {

    private static boolean checkIfAnagram(String s1, String s2){
        Map<Character, Integer> countMap = new HashMap<>();

        if(s1.length()!=s2.length())
            return false;

        for(int i=0; i<s1.length();i++){
            //increase for first string
            if(countMap.containsKey(s1.charAt(i))){
                int c=countMap.get(s1.charAt(i));
                countMap.put(s1.charAt(i), ++c);
            }
            else {
                countMap.put(s1.charAt(i), 1);
            }
            //decrease for second string
            if(countMap.containsKey(s2.charAt(i))){
                int c=countMap.get(s2.charAt(i));
                countMap.put(s2.charAt(i), --c);
            }
            else {
                countMap.put(s2.charAt(i), -1);
            }
        }

        for(int c :countMap.values()){
            if(c!=0)
                return false;
        }

        return true;
    }

    public static void main(String[] args) {
        System.out.println(checkIfAnagram("geeksforgeeks", "forgeeksgeeks"));
        System.out.println(checkIfAnagram("geeksforgeeks", "geeks"));
        System.out.println(checkIfAnagram("silent", "sitlee"));

    }
}
