package misc.playground.gfg;

import java.util.HashMap;
import java.util.Map;

public class FormPalindrome {

    private static int formPalindrome(String s){
        Map<Character, Integer> countMap = new HashMap<>();
        int count=0;
        for(int i=0; i<s.length();i++) {

            if (countMap.containsKey(s.charAt(i))) {
                int c = countMap.get(s.charAt(i));
                countMap.put(s.charAt(i), ++c);
            } else {
                countMap.put(s.charAt(i), 1);
            }
        }

        for(int c: countMap.values()){
            if(c%2 != 0)
                count++;
        }

        return count==0?0:count-1;
    }

    public static void main(String[] args) {
        //0 means it can form palindrome, >=1 means it can't
        System.out.println(formPalindrome("bab"));
        System.out.println(formPalindrome("aa"));
        System.out.println(formPalindrome("dcbabcd"));
        System.out.println(formPalindrome("dcbabcdfrt"));

    }
}
