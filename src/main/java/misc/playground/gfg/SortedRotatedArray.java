package misc.playground.gfg;

public class SortedRotatedArray {

    public static int search(int[] arr, int l, int h, int key) {
        if (l > h)
            return -1;
        int mid = (l + h) / 2;

        if (arr[mid] == key)
            return mid;

        //first half is sorted
        if (arr[l] <= arr[mid]) {
            if (key >= arr[l] && key <= arr[mid])
                return search(arr, l, mid - 1, key);
            else
                return search(arr, mid + 1, h, key);
        } else {
            if (key >= arr[mid] && key <= arr[l])
                return search(arr, mid + 1, h, key);
            else
                return search(arr, l, mid - 1, key);
        }

    }

    public static void main(String[] args) {
        int[] arr = {5, 6, 7 ,8, 9, 10, 11, 1, 2 ,3};
        int n=arr.length;
        int key=8;

        System.out.println(search(arr, 0, n-1, 11));
    }
}
