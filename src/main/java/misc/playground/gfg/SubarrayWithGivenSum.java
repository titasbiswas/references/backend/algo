package misc.playground.gfg;

import java.util.ArrayList;
import java.util.List;

public class SubarrayWithGivenSum {

    public static void main(String[] args) {

        int arr[] = {15, 2, 4, 8, 9, 5, 10, 23};
        int sum = 23; // 1..4

        contigousSum(arr, sum);

        int arr2[] = {3, 4, 5};
        List<Integer> lst = new ArrayList<>();
        lst.add(3);
        lst.add(4);
        lst.add(5);
        System.out.println(contigousProduct(lst, 5));



    }

    public static void contigousSum(int[] input, int checkSum) {
        int length = input.length, j = 0, currSum = 0;

        for (int i = 0; i < length; i++) {

            currSum += input[i];


            while (currSum > checkSum && j < i - 1) {
                currSum -= input[j];
                j++;
            }

            if (currSum == checkSum)
                System.out.println(j + " " + i);

        }
    }

    public static long contigousProduct(List<Integer> numbers, int k) {
        int n = numbers.size();
        long p = 1;
        int count = 0;
        for (int i = 0, j = 0; j < n; j++) {


            p *= numbers.get(j);

            while (i < j && p >= k)
                p /= numbers.get(i++);

            if (p <= k) {
                int l = j - i + 1;
                count += l;
            }
        }

        return count;
    }



}
