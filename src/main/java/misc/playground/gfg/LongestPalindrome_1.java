package misc.playground.gfg;

public class LongestPalindrome_1 {

    public static void main(String[] args) {
        findLongestPalindrome("forgeeksskeegfor");
    }

    public static void findLongestPalindrome(String str){

        int maxLength =0, strLength = str.length(), low=0, high=0, start = 0;

        for(int i=1;i<strLength;i++){

            //even length palindrome
            low = i-1;
            high = i;

            while(low>=0 && high<strLength && str.charAt(low)==str.charAt(high)){
                if(high+1-low>maxLength){
                    maxLength = high+1-low;
                    start =low;
                }
                high++;
                low--;
            }

            //odd length palindrome
            low=i-1;
            high=i+1;

            while (low>=0 && high<strLength && str.charAt(low)==str.charAt(high)){
                if(high+1-low>maxLength){
                    maxLength = high+1-low;
                    start =low;
                }
                high++;
                low--;
            }


        }

        System.out.println(str.substring(start, start+maxLength));

    }
}
