package misc.playground.gfg;

public class LargestContigiousSubArrSum_Array {

    public static int maxSubArraySum(int[] arr){
        int maxTillNow = 0, maxEndingHere=0;

        for (int i=0; i<arr.length;i++){
            maxEndingHere=maxEndingHere+arr[i];
            if (maxEndingHere<0)
                maxEndingHere=0;
            else if(maxTillNow<maxEndingHere)
                maxTillNow=maxEndingHere;
        }

        return maxTillNow;
    }

    public static int maxSubArraySum_negativeValues(int[] arr){
        int currMax=arr[0], maxSoFar=arr[0];
        for (int i=1;i<arr.length;i++){
            currMax= Math.max(arr[i], currMax+arr[i]);
            maxSoFar=Math.max(maxSoFar, currMax);
        }

        return currMax;
    }


    public static void main(String[] args)
    {
        int a[] = {-2, -3, 4, -1, -2, 1, 5, -3};
        int max_sum = maxSubArraySum(a);
        System.out.println("Maximum contiguous sum is "
                + max_sum);

        int arr[] = {-2, -3, -4, -1, -2, -1, -5, -3};
        int max_sum_n = maxSubArraySum_negativeValues(arr);
        System.out.println("Maximum contiguous sum is "
                + max_sum_n);
    }
}
