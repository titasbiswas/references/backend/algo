package misc.playground.gfg;

public class ReccursiveRemovalDuplicates {

    public static String removeUtil(String str, char last_removed) {
        if (str.length() == 0 || str.length() == 1) {
            return str;
        }

        if (str.charAt(0) == str.charAt(1)) {
            last_removed = str.charAt(0);
            while (str.length() > 1 && str.charAt(0) == str.charAt(1)) {
                str = str.substring(1, str.length());
            }
            //removing the first char which has been stored in last_removed
            str = str.substring(1, str.length());
            //recur for the rest of the string
            return removeUtil(str, last_removed);
        }

        //first char is different form its adjacent

        String rem_string = removeUtil(str.substring(1,str.length()), last_removed);

        if (rem_string.length() != 0 && rem_string.charAt(0) == str.charAt(0)) {
            last_removed = str.charAt(0);
            return rem_string.substring(1, rem_string.length());
        }

        if (rem_string.length() == 0 && last_removed == str.charAt(0)) {
            return rem_string;
        }

        return str.charAt(0)+rem_string;
    }

    static String remove(String str) {
        char last_removed = '\0';
        return removeUtil(str, last_removed);
    }

    // Driver code
    public static void main(String args[]) {
        String str1 = "geeksforgeeg";
        System.out.println(remove(str1));

        String str2 = "azxxxzy";
        System.out.println(remove(str2));

        String str3 = "caaabbbaac";
        System.out.println(remove(str3));

        String str4 = "gghhg";
        System.out.println(remove(str4));

        String str5 = "aaaacddddcappp";
        System.out.println(remove(str5));

        String str6 = "aaaaaaaaaa";
        System.out.println(remove(str6));

        String str7 = "qpaaaaadaaaaadprq";
        System.out.println(remove(str7));

        String str8 = "acaaabbbacdddd";
        System.out.println(remove(str8));

    }
}

