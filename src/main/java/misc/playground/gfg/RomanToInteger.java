package misc.playground.gfg;

import java.util.HashMap;
import java.util.Map;

public class RomanToInteger {

    private static Map<Character, Integer> romanToIntMap = new HashMap<>();

    private static void initMap(){
        romanToIntMap.put('I', 1);
        romanToIntMap.put('V', 5);
        romanToIntMap.put('X', 10);
        romanToIntMap.put('L', 50);
        romanToIntMap.put('C', 100);
        romanToIntMap.put('D', 500);
        romanToIntMap.put('M', 1000);


    }

    private static int toInteger(String roman){
        int result=0;
        for(int i=0;i<roman.length();i++){
            int curr = romanToIntMap.get(roman.charAt(i));
            if(i+1<roman.length()){
                int next = romanToIntMap.get(roman.charAt(i+1));
                if(curr>=next){
                    result+=curr;
                }
                else {
                    result+=(next-curr);
                    i++;
                }
            }
            else {
                result+=curr;
                i++;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        initMap();
        System.out.println(toInteger("MCMIV"));
    }
}
