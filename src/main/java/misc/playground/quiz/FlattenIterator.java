package misc.playground.quiz;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class FlattenIterator {
//    interface Iterator<E> {
//        boolean hasNext();
//        E next();
//        void remove();
//    }

    static class FlatIterator<E> implements Iterator<E> {
        Iterator<Iterator<E>> nestedIterator;
        Iterator<E> elementIter;
        FlatIterator(Iterator<Iterator<E>> nestedIterator) {
            if (nestedIterator == null) {
                throw new IllegalArgumentException("Input Iterator is null!");
            }
            if (!nestedIterator.hasNext()) {
                return;
            }
            this.nestedIterator = nestedIterator;
            this.elementIter = nestedIterator.next();
        }


        @Override
        public boolean hasNext() {
            if(elementIter.hasNext()){
                return true;
            }
            else{
                if(nestedIterator.hasNext()){
                    this.elementIter = nestedIterator.next();
                    return hasNext();
                }
            }
            return false;
        }

        @Override
        public E next() {
            return elementIter.next();
        }

        @Override
        public void remove() {

        }
    }


    public static void main(String[] args) {
        List<Integer> list1 = Arrays.asList(1, 2, 3);
        List<Integer> list2 = Arrays.asList(4, 5, 6);
        List<Integer> list3 = Arrays.asList(7, 8, 9);
        List<Iterator<Integer>> list = new ArrayList<>();
        list.add(list1.iterator());
        list.add(list2.iterator());
        list.add(list3.iterator());
        FlatIterator<Integer> flatIterator = new FlatIterator<Integer>(list.iterator());

        while(flatIterator.hasNext()) {
            System.out.println(flatIterator.next());//1 ..9
        }


    }

}
