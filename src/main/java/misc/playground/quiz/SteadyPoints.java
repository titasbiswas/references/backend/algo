package misc.playground.quiz;

//the input array represents distance after unit time from origin
//need to calculate the number of times the particle has been steady
//i.e constant speed or no movment, al least for three points
public class SteadyPoints {

    public static int count(int [] input){

        int inputSize = input.length;
        if(inputSize<3)
            return 0;
        int[] gaps = new int[inputSize-1];

        for (int i=0; i<inputSize-1;i++){
            gaps[i] = input[i+1]-input[i];
        }
        int count=0, currGap=gaps[0], currConCout=0;
        //int i=1;
        for(int i=1;i<gaps.length-3;i++){
            if(gaps[i]==currGap){
                currConCout++;
            }
            else{
                currGap = gaps[i];
                currConCout = 0;
            }

            if(currConCout==1){
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {

        int [] arr = {1,2,4,6,7,7,7,8,9,10,11,12};
        System.out.println(count(arr));
    }
}
