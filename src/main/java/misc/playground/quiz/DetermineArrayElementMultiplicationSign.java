package misc.playground.quiz;

public class DetermineArrayElementMultiplicationSign {
    public static void main(String[] args) {

        int [] arr1 = {-1, -2, -3, 5};
        int [] arr2 = {1, 2, 3, -5};
        int [] arr3 = {1, 2, 0, -5};
        System.out.println(solution(arr1));
        System.out.println(solution(arr2));
        System.out.println(solution(arr3));
    }

    public static int solution(int [] A){
        if(A.length == 0)
            return 0;

        int sign = 1;
        for(int i: A){
            if(i == 0)
                return 0;
            else if( i< 0){
                sign *= -1;
            }
        }
        return  sign;
    }
}
