package misc.playground.quiz;

import java.util.stream.IntStream;

public class CountPairs {

    public static void main(String[] args) {

        Long st1 = System.currentTimeMillis();
        System.out.println(solOne(1, 1000000000));
        System.out.println(System.currentTimeMillis() - st1);

        Long st2 = System.currentTimeMillis();
        System.out.println(solTwo(1, 1000000000));
        System.out.println(System.currentTimeMillis() - st2);


    }

    public static int solOne(int A, int B) {
        int a = (int) Math.sqrt(A), b = (int) Math.sqrt(B), x = 0, count = 0;

        for (int i = a; i <= b; i++) {
            x = i * (i + 1);
            if (x >= A && x <= B) {
                count++;
            }
        }
        return count;
    }

    public static int solTwo(int A, int B) {

        return (int) IntStream.range((int) Math.sqrt(A), (int) Math.sqrt(B) + 1)
                .filter(i -> i * (i + 1) <= B && i * (i + 1) >= A)
                .count();
    }
}
