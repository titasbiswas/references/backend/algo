package misc.playground.quiz;

import java.util.*;

public class ProductSort {

    public static void main(String[] args) {
        List<Integer> in1 = Arrays.asList(4, 5, 6, 5, 4, 3);

        itemsSort(in1); //364455
    }

    public static List<Integer> itemsSort(List<Integer> items) {
        // Write your code here
        Map<Integer, Integer> countMap = new HashMap<Integer, Integer>();
        items.stream().forEach(e ->{
            countMap.merge(e, 1, Integer::sum);
        });

        List<Integer> result = new ArrayList<>();
                countMap.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry<Integer, Integer>::getValue)
                        .thenComparing(Map.Entry::getKey))
                .forEach(e -> {
                    for (int i=0;i<e.getValue();i++){
                        result.add(e.getKey());
                    }
                });

        return result;

    }
}

