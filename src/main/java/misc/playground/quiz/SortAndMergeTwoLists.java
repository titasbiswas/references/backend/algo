package misc.playground.quiz;

import java.util.*;

public class SortAndMergeTwoLists {
    public static void main(String[] args) {
        List<String> list1 = Arrays.asList("a", "e", "g", "h");
        List<String> list2 = Arrays.asList("b", "c", "d", "f", "i", "j");


        System.out.println(bruteMethod(list1, list2));

    }

    private static List<String> bruteMethod(List<String> list1, List<String> list2) {
        int len1 = list1.size();
        int len2 = list2.size();
        List<String> result = new ArrayList<>();
        int i = 0, j = 0, k = 0;

        while (i < len1 && j < len2) {
            if (list1.get(i).compareTo(list2.get(j)) < 0) {
                result.add(k++, list1.get(i++));
            } else {
                result.add(k++, list2.get(j++));
            }
        }

        while (i < len1) {
            result.add(k++, list1.get(i++));
        }

        while (j < len2) {
            result.add(k++, list2.get(j++));
        }

        return result;
    }

    private static List<String> mapMethod(List<String> list1, List<String> list2) {

        Map<String, Integer> countMap = new HashMap<>();

        for (String e : list1) {
            if (countMap.containsKey(e)) {

            }
            //countMap.computeIfPresent(e, (k, v) => {

            //})
        }
        return null;
    }
}
