package misc.playground.quiz;

import java.util.stream.IntStream;

public class LowestNumberSameDigit {

    public static void main(String[] args) {
        Long st1 = System.currentTimeMillis();
        System.out.println(firstSol(999999999));
        IntStream.range(1, 10000).forEach(i -> {
            firstSol(999999999);
        });
        System.out.println(System.currentTimeMillis() - st1);

        Long st2 = System.currentTimeMillis();
        System.out.println(secondSol(999999999));
        IntStream.range(1, 10000).forEach(i -> {
            secondSol(999999999);
        });
        System.out.println(System.currentTimeMillis() - st2);


    }


    public static int firstSol(int n) {
        int i = 1;

        if (n / 10 == 0)
            return 0;

        while (n / 10 > 0) {
            i *= 10;
            n /= 10;
        }

        return i;
    }


    public static int secondSol(int N) {
        if (N / 10 == 0)
            return 0;
        return (int) Math.pow(10, String.valueOf(N).length() - 1);
    }

}
