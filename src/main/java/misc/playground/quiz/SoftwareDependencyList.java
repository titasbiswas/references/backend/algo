package misc.playground.quiz;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Handle requests for multiple objects at once. In this example we are asking for objects Gnome 3 and libelf++. Return a single list of which contains dependencies for both objects without duplicates.
 *
 * [Boost, Cairo 2D, GTK Widget Toolkit, Gnome 3, libelf++] would be a correct solution.
 * [Boost, Cairo 2D, libelf++, GTK Widget Toolkit, Gnome 3] would also be a correct solution.
 * */


public class SoftwareDependencyList {

    public static void main(String[] args) {

        // software dependency map
        Map<String, String> dependencyMap = new HashMap<>();
        dependencyMap.put("Gnome 3", "GTK Widget Toolkit");
        dependencyMap.put("Boost", null);
        dependencyMap.put("elfutils", "libelf++");
        dependencyMap.put("libelf++", "Cairo 2D");
        dependencyMap.put("GTK Widget Toolkit", "Cairo 2D");
        dependencyMap.put("GDB", "elfutils");
        dependencyMap.put("Cairo 2D", "Boost");
        System.out.println(dependencyMap);

        String software = "Gnome 3";

        List<String> softwares = Arrays.asList("Gnome 3", "elfutils");


        System.out.println(getDependencyList(dependencyMap, softwares));
    }


    public static Collection<String> getDependencyList(Map<String, String> dependencyMap, List<String> softwares) {

        Set<String> set = new HashSet<>();
        LinkedList<String> dList;
        List<List<String>> result = new ArrayList<>();
        for (String software : softwares) {
            dList = new LinkedList<String>();
            while (software != null && !set.contains(software)) {
                set.add(software);
                dList.push(software);
                software = dependencyMap.get(software);

            }
            result.add(dList);

        }

        return result.stream().flatMap(list ->  list.stream())
                                    .collect(Collectors.toList());
    }
}
