package misc.playground.quiz;

import java.util.Arrays;

public class CommonStringFragment {

    static boolean hasCommonFragment(String s1, String s2)
    {

        boolean v[]=new boolean[26];
        Arrays.fill(v,false);

        for (int i = 0; i < s1.length(); i++)
            v[s1.charAt(i) - 'a'] = true;


        for (int i = 0; i < s2.length(); i++)
            if (v[s2.charAt(i) - 'a'])
                return true;

        return false;
    }

    public static void main(String[] args) {
        String str1 = "hello";
        String str2 = "world";
        if (hasCommonFragment(str1, str2))
            System.out.print("Yes");
        else
            System.out.print("No");
    }
}
