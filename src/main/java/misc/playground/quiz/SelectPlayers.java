package misc.playground.quiz;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

enum SelectedStat {
    SELECT, REJECT;
}

enum PlayerType {
    STRIKER, DEFENDER, NA;
}

public class SelectPlayers {

    public static void main(String[] args) {

    }

    public static List<List<String>> getSelectionStatus(List<List<String>> applications) {
        final double minHeight = 5.8;
        final int maxBmi = 23;
        final int minGoalScored = 50;
        final int minGoalDefended = 30;

        List<List<String>> team = new ArrayList<>();

        List<Player> classifiedPlayers =
                applications.stream().map(playerStat -> {
                    List<Player> playerList = new ArrayList<>();
                    String name = playerStat.get(0);
                    double height = Double.valueOf(playerStat.get(1));
                    int bmi = Integer.valueOf(playerStat.get(2));
                    int score = Integer.valueOf(playerStat.get(3));
                    int defend = Integer.valueOf(playerStat.get(4));
                    Player player = new Player(name, height, bmi, score, defend);

                    if (player.height >= minHeight && player.bmi <= maxBmi &&
                            (player.score >= minGoalScored || player.defend >= minGoalDefended)) {
                        player.setSelected(SelectedStat.SELECT);
                    } else {
                        player.setSelected(SelectedStat.REJECT);
                    }

                    if (player.getSelected() == SelectedStat.REJECT) {
                        player.setType(PlayerType.NA);
                    } else {
                        if (player.getDefend() > player.getScore())
                            player.setType(PlayerType.DEFENDER);
                        else
                            player.setType(PlayerType.STRIKER);
                    }
                    return player;

                }).collect(Collectors.toList());

        long strikerCount = classifiedPlayers.stream()
                .filter(player -> player.type == PlayerType.STRIKER).count();
        long defenderCount = classifiedPlayers.stream()
                .filter(player -> player.type == PlayerType.DEFENDER).count();
        if ((strikerCount == 0) || (defenderCount == 0))
            return team;
        long minPartition = strikerCount <= defenderCount ? strikerCount : defenderCount;

        List<Player> sortedStriker = classifiedPlayers.stream()
                .filter(player -> player.type == PlayerType.STRIKER)
                .sorted(Comparator.comparing(Player::getScore).reversed()
                        .thenComparing(Comparator.comparing(Player::getHeight).reversed())
                        .thenComparing(Comparator.comparing(Player::getBmi))
                )
                .limit(minPartition)
                .collect(Collectors.toList());

        List<Player> sortedDefender = classifiedPlayers.stream()
                .filter(player -> player.type == PlayerType.DEFENDER)
                .sorted(Comparator.comparing(Player::getDefend).reversed()
                        .thenComparing(Comparator.comparing(Player::getHeight).reversed())
                        .thenComparing(Comparator.comparing(Player::getBmi))
                )
                .limit(minPartition)
                .collect(Collectors.toList());
        List<Player> tempLis = new ArrayList<>();
        tempLis.addAll(sortedDefender);
        tempLis.addAll(sortedStriker);

        for (Player p : classifiedPlayers) {
            List<String> list = new ArrayList<>();
            if (tempLis.contains(p)) {
                list.add(p.getName());
                list.add(p.getSelected().toString());
                list.add(p.getType().toString());
            } else {
                list.add(p.getName());
                list.add(p.getSelected().toString());
                list.add(PlayerType.NA.toString());
            }
            team.add(list);
        }
        //team.sort();


        return team.stream().sorted(Comparator.comparing(o -> o.get(0))).collect(Collectors.toList());


    }


}
class Player {
    String name;
    double height;
    int bmi;
    int score;
    int defend;
    PlayerType type;
    SelectedStat selected;


    public Player(String name, double height, int bmi, int score, int defend) {
        this.name = name;
        this.height = height;
        this.bmi = bmi;
        this.score = score;
        this.defend = defend;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getBmi() {
        return bmi;
    }

    public void setBmi(int bmi) {
        this.bmi = bmi;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getDefend() {
        return defend;
    }

    public void setDefend(int defend) {
        this.defend = defend;
    }

    public PlayerType getType() {
        return type;
    }

    public void setType(PlayerType type) {
        this.type = type;
    }

    public SelectedStat getSelected() {
        return selected;
    }

    public void setSelected(SelectedStat selected) {
        this.selected = selected;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Double.compare(player.height, height) == 0 &&
                bmi == player.bmi &&
                score == player.score &&
                defend == player.defend &&
                Objects.equals(name, player.name) &&
                type == player.type &&
                selected == player.selected;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, height, bmi, score, defend, type, selected);
    }


}