package misc.playground.quiz;

public class MaxOccurence {

    int solution(int M, int[] A) {
        int N = A.length;
        int[] count = new int[M + 1];
        for (int i = 0; i <= M; i++)
            count[i] = 0;
        int maxOccurence = 1;
        int index = 0;
        for (int i = 0; i < N; i++) {
            if (count[A[i]] > 0) {
                int tmp = count[i];
                if (tmp > maxOccurence) {
                    maxOccurence = tmp;
                    index = i;
                }
                count[A[i]] = tmp + 1;
            } else {
                count[A[i]] = 1;
            }
        }
        return A[index];
    }

    public static void main(String[] args) {
        MaxOccurence maxOccurence = new MaxOccurence();
        int[] arr= {0};
        System.out.println(maxOccurence.solution(0, arr));
    }
}
