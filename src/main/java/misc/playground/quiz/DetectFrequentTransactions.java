package misc.playground.quiz;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class DetectFrequentTransactions {

    public static void main(String[] args) {
        List<String> logs = new ArrayList<>();
        logs.add("88 99 200");
        logs.add("88 99 300");
        logs.add("99 32 100");
        logs.add("12 12 15");

        processLogs(logs, 2).forEach(System.out::println);
    }

    private static List<String> processLogs(List<String> logs, int threshold) {

        Map<Integer, Integer> map = new HashMap<>();

        BiFunction<Integer, Integer, Integer> defaultAdder = (k, v)-> {
            if(v==null)
                return 1;
            return v+1;
        };

        logs.stream().forEach(log -> {
            Integer[] arr = Arrays.stream(log.split(" ")).map(i-> Integer.valueOf(i)).toArray(Integer[]::new);;
            if(arr[0].equals(arr[1])){
                //using merge
                map.merge(arr[0], 1, Integer::sum);
            }else{
                //using in place lambda
                map.compute(arr[0], (k,v)->v==null?1:v+1);
                //using bifunction
                map.compute(arr[1], defaultAdder);
            }
        });

        return map.entrySet().stream()
                .filter(k -> k.getValue() >= threshold)
                .sorted((Map.Entry.comparingByKey()))
                .map(Map.Entry::getKey)
                .map(e->e.toString())
                .collect(Collectors.toList());

    }
}
