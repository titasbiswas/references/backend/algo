package misc.playground.quiz;

import java.util.ArrayList;
import java.util.List;

public class MissingWords {

    public static void main(String[] args) {
        String s = "I am using hackerrank to improve programming";
        String t = "am hackerrank to improve";
        missingWords(s, t);
    }

    public static List<String> missingWords(String s, String t) {
        // Write your code here

//        List<String> testWords = Arrays.stream(t.split(" ")).collect(Collectors.toList());
//        List<String> relsult = Arrays.stream(s.split(" ")).filter(e -> !testWords.contains(e)).collect(Collectors.toList());
//        return relsult;

        String[] aa = s.split(" ");
        String[] bb = t.split(" ");
        List<String> result = new ArrayList<>();
        int j = 0;
        int i = 0;
        for (; i < aa.length && j < bb.length; i++) {
            if (aa[i].equals(bb[j])) {
                j++;
                continue;
            }
            result.add(aa[i]);
        }

        while (i < aa.length) {
            result.add(aa[i++]);
        }
        return result;
    }
}
