package misc.playground.quiz;

public class BodmasRule {

    public static String bRule(String str) {
        StringBuilder result = new StringBuilder();
        boolean neg = false, inBracket = false;
        for (int i = 0; i < str.length(); i++) {
            char t = str.charAt(i);
            if (t == ' ')
                continue;


            if (neg && inBracket) {
                if (t == '-')
                    t = '+';
                else if (t == '+')
                    t = '-';
            }

            if (t == '(' || t == ')') {
                if (t == '(')
                    inBracket = true;
                if (t == ')')
                    inBracket = false;
                continue;
            } else {
                if (t == '-')
                    neg = true;
                if (t == '+')
                    neg = false;
                result.append(String.valueOf(t));
            }

        }
        return result.toString();
    }

    public static void main(String[] args) {
        System.out.println(bRule("a+(b - c) - (d - e) - (f+g)+ (h+i)"));//a+b-c-d+e-f-g+h+i
    }
}
