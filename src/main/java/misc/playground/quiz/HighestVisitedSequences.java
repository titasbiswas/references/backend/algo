package misc.playground.quiz;

import java.util.HashMap;
import java.util.Map;

//[1,3,2,4,5,1,3,2]
public class HighestVisitedSequences {

    public static String getHignestVisitedProduct(String[] visitedProducts, int desiredVisitLength){

        String highKey = ""; int highCount = 1;
        Map<String, Integer> countMap= new HashMap<>();
        for(int i=0;i<desiredVisitLength;i++){
            highKey = highKey+visitedProducts[i];
        }

        countMap.put(highKey, 1);

        for(int i = 1; i < visitedProducts.length-desiredVisitLength; i++){
            String key = "";
            for(int j=i;j<i+desiredVisitLength;j++){
                key = key+visitedProducts[j];
            }


            if(countMap.containsKey(key)){
                int c= countMap.get(key)+1;
                countMap.put(key, c);
                if(highCount<c){
                    highCount=c;
                    highKey=key;
                }
            }
            else {
                countMap.put(key, 1);
            }

        }

        return highKey;
    }

    public static void main(String[] args) {
        String input[] = {"1","3","2","4","5","3","2", "4", "1"};
       String result =  getHignestVisitedProduct(input, 3);
       System.out.println(result);
    }
}
