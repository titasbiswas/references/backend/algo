package misc.playground.quiz;

public class SumOfOnes_BinSearch {
    public static void main(String[] args) {
        int[] arr = {1,1,1,1,1,1};
        System.out.println(sum(arr));
    }
    static int sum(int[] arr){
        if(arr[arr.length-1]==0){
            return 0;
        }
        if(arr[0]==1){
            return arr.length;
        }
        int len = arr.length;
        int lastIndex = searchLastZero(arr, arr[0], len-1);
        System.out.println(lastIndex);
        int sum = arr.length-1  - lastIndex;
        return sum;
    }

    private static int searchLastZero(int[] arr, int low, int high) {
        int mid = low+(high-low)/2;
        if(low>high){
            return high;
        }
        if(arr[mid]==0){
            return searchLastZero(arr, mid+1, high);
        }
        else{
            return searchLastZero(arr, low, mid-1);
        }
    }
}
