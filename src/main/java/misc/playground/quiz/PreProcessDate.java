package misc.playground.quiz;

import java.util.*;
import java.util.function.Function;

public class PreProcessDate {

    public static void main(String[] args) {
        String[] dates = {"1st Mar 1974", "22nd sep 2001"};
        System.out.println(Arrays.asList(preProcessdate(dates)));
    }

    static String[] preProcessdate(String[] dates){

        final Map<String, String> monthMap  = new HashMap<>();
        monthMap.put("JAN", "01");
        monthMap.put("FEB", "02");
        monthMap.put("MAR", "03");
        monthMap.put("APR", "04");
        monthMap.put("MAY", "05");
        monthMap.put("JUN", "06");
        monthMap.put("JUL", "07");
        monthMap.put("AUG", "08");
        monthMap.put("SEP", "09");
        monthMap.put("OCT", "10");
        monthMap.put("NOV", "11");
        monthMap.put("DEC", "12");

        Function<String, String> preConverter = input -> {
            StringTokenizer tokenizer = new StringTokenizer(input, " ");
            ArrayList<String> tokens = new ArrayList<>(3);
            while (tokenizer.hasMoreElements()) {
                tokens.add(tokenizer.nextToken());
            }
            //append year
            StringBuilder dateBuilder = new StringBuilder(tokens.get(2));
            //append month
            String tempMonth  = monthMap.get(tokens.get(1).toUpperCase());
            dateBuilder.append("-").append(tempMonth);
            //apped day
            String tempDay = tokens.get(0).substring(0, tokens.get(0).length()-2);
            dateBuilder.append("-").append(tempDay.length() == 2? tempDay:"0"+tempDay);

            return  dateBuilder.toString();
        };

        return Arrays.stream(dates).map(preConverter).toArray(String[]::new);



    }
}
