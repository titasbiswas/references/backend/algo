package misc.playground.quiz;

import java.util.*;

public class KnapSack_WrongApproach {
    public static void main(String[] args) {
        //Map<String, Integer> map = new HashMap<>();
        List<List<Integer>> cost = new ArrayList<>();
        cost.add(Arrays.asList(1, 2, 3));
        cost.add(Arrays.asList(1, 2, 3));
        cost.add(Arrays.asList(3, 3, 1));
        System.out.println(minCost(cost));

        final Map<String, Integer> wordCounts = new HashMap<>();
        wordCounts.put("USA", 100);
        wordCounts.put("jobs", 200);
        wordCounts.put("software", 50);
        wordCounts.put("technology", 70);
        wordCounts.put("opportunity", 200);

        System.out.println(wordCounts.entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .findFirst()
                .map(e -> e.getValue())
                .get()
        );


    }

    public static int minCost(List<List<Integer>> cost) {
        int lastMinIndex = -1, totalCost = 0, s = cost.size();
        if (s == 0)
            return 0;

        for (List<Integer> r : cost) {
            int currMinCost = Integer.MAX_VALUE;
            //int l = r.size();
            for (int i = 0; i < 3; i++) {
                if (r.get(i) < currMinCost && i != lastMinIndex) {
                    currMinCost = r.get(i);
                    lastMinIndex = i;
                }
            }
            totalCost += currMinCost;
        }
        return totalCost;
    }


}
