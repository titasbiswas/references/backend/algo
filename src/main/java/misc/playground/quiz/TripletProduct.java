package misc.playground.quiz;

import java.util.*;
import java.util.stream.Collectors;

//select distinct triplets with product of the  desiredCapacity, two should be adjacent
public class TripletProduct {

    public static void main(String[] args) {
        int[] capacity = {1, 3, 5, 3, 5};
        long desiredCapacity = 15;
        List<Integer> list = Arrays.stream(capacity)
                .boxed()
                .collect(Collectors.toList());
        System.out.println(totalTriplets(list, desiredCapacity));

    }

    public static long totalTriplets(List<Integer> capacity, long desiredCapacity) {
        // Write your code here

        Set<String> countSet = new HashSet<>();
        int len = capacity.size();


        for (int i = 0; i < len - 1; i++) {
            long temp = capacity.get(i) * capacity.get(i + 1);
            if(desiredCapacity%temp != 0)
                continue;

            for (int j = 0; j < len; j++) {
                if(j != i && j!= i+1) {
                    if (desiredCapacity == Long.valueOf(capacity.get(j)) * Long.valueOf(temp)) {
                        //countSet.add(i + "" + (i + 1) + "" + j);

                        if(countSet.contains(i + "" +j) || countSet.contains(j + "" +i)) {
                           continue;
                        }
                        countSet.add(i + "" + j);

                    }
                }
            }
        }
        return countSet.size();
    }
}
