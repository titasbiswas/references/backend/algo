package misc.playground.quiz;

import lombok.Data;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class ProductionLineTester {
    private final ProductVerifier verifier;

    ProductionLineTester(ProductVerifier verifier) {
        this.verifier = verifier;
    }

    ProductLineTestReport test(Stream<Product> products) {
        final AtomicInteger checkedCount = new AtomicInteger(0);
        final AtomicInteger uncheckedCount = new AtomicInteger(0);
        final AtomicInteger errorCount = new AtomicInteger(0);
        final AtomicInteger correctCount = new AtomicInteger(0);
        if(Objects.nonNull(products)) {
            products.filter(product -> product!= null && !"invalid".equalsIgnoreCase(product.getStatus()))
                    .skip(10)
                    .limit(20)
                    .forEach(p -> {
                        try {
                            verifier.verify(p);
                            correctCount.incrementAndGet();
                        }  catch (RuntimeException e) {
                            uncheckedCount.incrementAndGet();
                        } catch (Exception e) {
                            checkedCount.incrementAndGet();
                        }catch (Error err) {
                            errorCount.incrementAndGet();
                        }

                    });
        }
        return new ProductLineTestReport(correctCount.get(), checkedCount.get(),uncheckedCount.get(),  errorCount.get());
    }

    private class ProductLineTestReport {
        public ProductLineTestReport(int i, int i1, int i2, int i3) {
        }
    }
}

class ProductVerifier{

    public void verify(Object p) {
    }
}

@Data
class Product {
    String status;
}
