package misc.playground.quiz; /**
 You are in charge of a display advertising program. Your ads are displayed on websites all over the internet. You have some CSV input data that counts how many times that users have clicked on an ad on each individual domain. Every line consists of a click count and a domain name, like this:

 counts = [ "900,google.com",
 "60,mail.yahoo.com", //60
 "10,mobile.sports.yahoo.com",
 "40,sports.yahoo.com",
 "300,yahoo.com",
 "10,stackoverflow.com",
 "20,overflow.com",
 "5,com.com",
 "2,en.wikipedia.org",
 "1,m.wikipedia.org",
 "1,mobile.sports",
 "1,google.co.uk"]

 Write a function that takes this input as a parameter and returns a data structure containing the number of clicks that were recorded on each domain AND each subdomain under it. For example, a click on "mail.yahoo.com" counts toward the totals for "mail.yahoo.com", "yahoo.com", and "com". (Subdomains are added to the left of their parent domain. So "mail" and "mail.yahoo" are not valid domains. Note that "mobile.sports" appears as a separate domain near the bottom of the input.)

 Sample output (in any order/format):

 calculateClicksByDomain(counts) =>
 com:                     1345
 google.com:              900
 stackoverflow.com:       10
 overflow.com:            20
 yahoo.com:               410
 mail.yahoo.com:          60
 mobile.sports.yahoo.com: 10
 sports.yahoo.com:        50
 com.com:                 5
 org:                     3
 wikipedia.org:           3
 en.wikipedia.org:        2
 m.wikipedia.org:         1
 mobile.sports:           1
 sports:                  1
 uk:                      1
 co.uk:                   1
 google.co.uk:            1

 n: number of domains in the input
 (individual domains and subdomains have a constant upper length)


 */


import java.util.HashMap;
import java.util.Map;

public class HitCount {
    public static void main(String[] argv) {


        String[] counts = {
                "900,google.com",
                "60,mail.yahoo.com",
                "10,mobile.sports.yahoo.com",
                "40,sports.yahoo.com",
                "300,yahoo.com",
                "10,stackoverflow.com",
                "20,overflow.com",
                "5,com.com",
                "2,en.wikipedia.org",
                "1,m.wikipedia.org",
                "1,mobile.sports",
                "1,google.co.uk"
        };

        Map<String, Integer> dm = initDomainHitMap(counts);
        dm.entrySet().stream().forEach(entry -> {
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        });
        System.out.println(dm);
        Map<String, Integer> hitMap = createHitCountMap(counts);

    }

    private static Map<String, Integer> initDomainHitMap(String [] hits){
        Map<String, Integer> cm = new HashMap<>();
        for(String hit: hits){
            String topdomain = hit.split(",")[1];
            Integer count = Integer.valueOf(hit.split(",")[0]);
            String temp="";
            String[] dTokens = topdomain.split("\\.");//mobile, sports, yahoo, com, mobile.sports.yahoo.com
            for(int i = dTokens.length-1; i>=0;i--){
                if(temp.isBlank()){
                    temp = dTokens[i];
                }
                else{
                    temp = dTokens[i]+"."+temp;
                }

                cm.merge(temp, count, Integer::sum);

            }

        }

        return cm;
    }

    private static Map<String, Integer> createHitCountMap(String [] hits){
        Map<String, Integer> cm = new HashMap<>();
        for(String hit: hits){
            String[] hitToken = hit.split(",");
            cm.put(hitToken[1], Integer.valueOf(hitToken[0]));
        }

        return cm;
    }
}

