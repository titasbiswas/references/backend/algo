package misc.playground.quiz;

class CountMinChangeToMaxSimilarValue {
    public static void main(String[] args) {
        int[] arr1 = {1,1,4,4,4,4,4,4};
        System.out.println(solution(arr1, 2));
    }

    static int solution(int[] A, int K) {
        int n = A.length;
        int best = 0;
        int count = 1;
        if(K == n) return K;
        for (int i = 0; i < n - K - 1; i++) {
            if (A[i] == A[i + 1])
                count = count + 1;
            else
                count = 1;
            if (count > best)
                best = count;
        }
        int result = best + K;

        return result;
    }
}
