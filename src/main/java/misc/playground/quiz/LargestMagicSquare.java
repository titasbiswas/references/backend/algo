package misc.playground.quiz;

public class LargestMagicSquare {


    static int largestMagicSquare(int[][] arr) {
        int col=arr[0].length, row=arr.length;
        int x=2, max=1;
        int maxX = col<row?row:col;
        for(;x<=maxX;x++){
            for(int i=0;i<=row-x;i++){
                for(int j=0;j<=col-x;j++){
                    if(isMagicSquare(arr,x, i, j)){
                        max=max>x?max:x;
                    }
                }
            }
        }

        return max;
    }



    static boolean isMagicSquare(int mat[][], int x, int r, int c) {
        //diagonal
        int sum = 0, sum2 = 0;
        for (int i = 0; i < x; i++)
            sum = sum + mat[i+r][i+c];

        for (int i = 0; i < x; i++)
            sum2 = sum2 + mat[i+r][x+c - 1 - i];

        if (sum != sum2)
            return false;

        // row
        for (int i = 0; i < x; i++) {
            int rowSum = 0;
            for (int j = 0; j < x; j++)
                rowSum += mat[i+r][j+c];
            if (rowSum != sum)
                return false;
        }

        // Columns
        for (int i = 0; i < x; i++) {
            int colSum = 0;
            for (int j = 0; j < x; j++)
                colSum += mat[j+r][i+c];

            if (sum != colSum)
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        int arr[][]= {
                {7,2,4},
                {2,7,6}, {9,5,1}, {4,3,8}
                , {3,5,4}
        };

        System.out.println(largestMagicSquare(arr));
    }
}
