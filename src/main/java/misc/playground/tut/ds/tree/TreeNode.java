package misc.playground.tut.ds.tree;

public class TreeNode {

    private int data;
    private int count;
    private TreeNode leftChild;
    private TreeNode rightChild;

    public TreeNode(int value) {
        count = 1;
        this.data = value;
    }

    public void insert(int value) {

        if (value == this.data) {
            count++;
            return;
        }
        if (value < this.data) {
            if (leftChild == null) {
                leftChild = new TreeNode(value);
            } else {
                leftChild.insert(value);
            }
        } else if (value > this.data) {
            if (rightChild == null) {
                rightChild = new TreeNode(value);
            } else {
                rightChild.insert(value);
            }
        }

    }

    public void traversalInOrder() {
        if (leftChild != null) {
            leftChild.traversalInOrder();
        }
        System.out.println("Data: " + data + " Count: " + count);
        if (rightChild != null) {
            rightChild.traversalInOrder();
        }
    }

    public TreeNode get(int value) {
        if (value == this.data) {
            return this;
        }
        if (value < this.data) {
            if (leftChild != null)
                return leftChild.get(value);
        } else {
            if (rightChild != null)
                return rightChild.get(value);
        }

        return null;
    }

    public TreeNode getMin(){
        if(leftChild == null){
            return this;
        }else {
            return leftChild.getMin();
        }
    }

    public TreeNode getMax(){
        if(rightChild == null){
            return this;
        }else {
            return rightChild.getMax();
        }
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public TreeNode getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(TreeNode leftChild) {
        this.leftChild = leftChild;
    }

    public TreeNode getRightChild() {
        return rightChild;
    }

    public void setRightChild(TreeNode rightChild) {
        this.rightChild = rightChild;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "data=" + data +
                ", count=" + count +
                '}';
    }
}
