package misc.playground.tut.ds.tree;

public class Tree {

    private TreeNode rootNode;


    public void insert(int value){
        if(rootNode == null){
            rootNode = new TreeNode(value);
        }else{
            rootNode.insert(value);
        }
    }

    public void traverseInOrder(){
        if(rootNode == null){
            return;
        }
        else {
            rootNode.traversalInOrder();
        }
    }

    public TreeNode get(int value){
        if(rootNode != null){
            return rootNode.get(value);
        }
        return null;
    }

    public TreeNode getMin(){
        if(rootNode==null){
            return new TreeNode(Integer.MIN_VALUE);
        }
        else {
            return rootNode.getMin();
        }
    }

    public TreeNode getMax(){
        if(rootNode==null){
            return new TreeNode(Integer.MAX_VALUE);
        }
        else {
            return rootNode.getMax();
        }
    }

    public void delete(int value){
        rootNode = delete(rootNode, value);
    }

    private TreeNode delete(TreeNode subTreeRoot, int value) {

        if(subTreeRoot==null){
            return subTreeRoot;
        }
        if(value<subTreeRoot.getData()){
            subTreeRoot.setLeftChild(delete(subTreeRoot.getLeftChild(), value));
        }
        else if(value>subTreeRoot.getData()){
            subTreeRoot.setRightChild(delete(subTreeRoot.getRightChild(), value));
        }else {
            if (subTreeRoot.getLeftChild() == null){
                return subTreeRoot.getRightChild();
            }else if (subTreeRoot.getRightChild() == null){
                return subTreeRoot.getLeftChild();
            }


            //This line requires to be revisited and refactored
            subTreeRoot.setData(subTreeRoot.getRightChild().getMin().getData());

            subTreeRoot.setRightChild(delete(subTreeRoot.getRightChild(), subTreeRoot.getData()));
        }

        return subTreeRoot;
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }
}
