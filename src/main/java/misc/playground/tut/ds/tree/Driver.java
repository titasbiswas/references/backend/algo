package misc.playground.tut.ds.tree;

public class Driver {

    public static void main(String[] args) {
        Tree intTree = new Tree();
        intTree.insert(25);
        intTree.insert(20);
        intTree.insert(15);
        intTree.insert(27);
        intTree.insert(30);
        intTree.insert(29);
        intTree.insert(26);
        intTree.insert(22);
        intTree.insert(27);
        intTree.insert(32);

        intTree.traverseInOrder();

       // System.out.println(intTree.get(27));
        //System.out.println(intTree.get(2744));


        //System.out.println((intTree.getMin()));
        //System.out.println((intTree.getMax()));

        intTree.delete(27);
        System.out.println("\n\n");
        intTree.traverseInOrder();
    }
}
