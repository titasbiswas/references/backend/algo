package misc.playground.tut.sorts;

import java.util.Arrays;

public class InsertionSort {
    public static void main(String[] args) {
        int[] arr = {20, 35, -15, 7, 55, 1, -22};

        for (int fui = 1; fui < arr.length; fui++) {
            int ne = arr[fui];
            int i = fui;
            for (; i > 0 && arr[i - 1] > ne; i--) {
                arr[i] = arr[i - 1];
            }
            arr[i] = ne;
        }

        Arrays.stream(arr).forEach(System.out::println);
    }
}
