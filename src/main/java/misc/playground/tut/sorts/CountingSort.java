package misc.playground.tut.sorts;

import java.util.Arrays;

public class CountingSort {
    public static void main(String[] args) {
        int[] intArray = { 2, 5, 9, 8, 2, 8, 7, 10, 4, 3 };
        countingSort(intArray, 1, 10);
        Arrays.stream(intArray).forEach(System.out::println);
    }

    public static void countingSort(int[] input, int min, int max){
         int countArr[] = new int[max-min+1];

         for(int i = 0; i<input.length;i++){
             countArr[input[i]-min]++;
        }

         int j=0;

         for( int i = min;i<=max;i++){
             while(countArr[i-min]>0){
                 input[j++]=i;
                 countArr[i-min]--;
             }
         }
    }
}
