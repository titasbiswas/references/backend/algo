package misc.playground.tut.sorts;

import java.util.Arrays;

public class ShellShort {

    public static void main(String[] args) {
        int[] arr = {20, 35, -15, 7, 55, 1, -22};


        for (int gap = arr.length / 2; gap > 0; gap /= 2) {

            for (int i = gap; i < arr.length; i++) {
                int ne = arr[i];

                int j = i;

                while (j >= gap && arr[j - gap] > ne) {
                    arr[j] = arr[j - gap];
                    j -= gap;
                }

                arr[j] = ne;
            }
        }

        Arrays.stream(arr).forEach(System.out::println);
    }
}
