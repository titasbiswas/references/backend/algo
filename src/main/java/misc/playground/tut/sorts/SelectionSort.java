package misc.playground.tut.sorts;

import java.util.Arrays;

public class SelectionSort {

    public static void main(String[] args) {
        int[] arr = {20, 35, -15, 7, 55, 1, -22};

        for (int uli = arr.length - 1; uli > 0; uli--) {
            int li = 0;
            for (int i = 1; i <= uli; i++) {
                if (arr[i] > arr[li])
                    li = i;
            }
            swap(arr, uli, li);
        }

        Arrays.stream(arr).forEach(System.out::println);
    }

    public static void swap(int[] arr, int i, int j) {
        if (i == j)
            return;

        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
