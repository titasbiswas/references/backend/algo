package misc.playground;

public class MySingleton {


    private MySingleton(){
    }

    private static class Initializer{
        private static final MySingleton INSTANCE = new MySingleton();

    }

    /**
     * The class loader loads classes when they are first accessed (in this case Holder's only access is within the getInstance() method).
     * When a class is loaded, and before anyone can use it, all static initializers are guaranteed to be executed (that's when Holder's static block fires).
     * The class loader has its own synchronization built right in that make the above two points guaranteed to be threadsafe.
     * */

    public static MySingleton getInstance(){
        return Initializer.INSTANCE;
    }
}
