package base.bit;

public class PowerOfTwo {
    /* Power of two only has one set bit
     * n & (n-1) always unset the right most set bit
     * 4= 100, 3 = 011  => 4&3 = 000
     * 6 = 110, 5 = 101 => 6&5 = 100*/
    public static void main(String[] args) {

        System.out.println(isPowTwo(6));
        System.out.println(isPowTwo(4));

    }

    public static boolean isPowTwo(int n) {
        if (n == 0)
            return false;
        return ((n & (n - 1)) == 0);
    }
}
