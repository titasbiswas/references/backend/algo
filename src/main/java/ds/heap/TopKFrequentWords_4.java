package ds.heap;

import java.util.*;

public class TopKFrequentWords_4 {
    public static void main(String[] args) {
        String[] words = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"};
        //String[] words = {"i","love","leetcode","i","love","coding"};
        int k = 4;
        System.out.println(solution(words, k)); //["the","is","sunny","day"]
    }

    private static List<String> solution(String[] words, int k){
        Map<String, Integer> countMap = new HashMap<>();
        for(String a : words){
            countMap.merge(a, 1, Integer::sum);
        }
        Comparator<Map.Entry<String, Integer>> comparator = (a,b) -> {
            if(a.getValue() != b.getValue()){
                return a.getValue() - b.getValue();
            }
            return -a.getKey().compareTo(b.getKey());
        };

        PriorityQueue<Map.Entry<String, Integer>> pq = new PriorityQueue<>(comparator);
        for(Map.Entry<String, Integer> e: countMap.entrySet()){
           pq.offer(e);
           while (pq.size() > k)
               pq.poll();
        }

        List<String> res = new ArrayList<>();
        while (!pq.isEmpty()){
            res.add(pq.poll().getKey());
        }
        Collections.reverse(res);
        return res;
    }
}

class Tuple implements Comparable<Tuple>{
    String value;
    int count;

    public Tuple(String value, int count) {
        this.value = value;
        this.count = count;
    }


    @Override
    public int compareTo(Tuple o) {
        if(count<o.count){
            return -1;
        }
        else if(count > o.count){
            return 1;
        }
        else {
            return value.compareTo(o.value);
        }
    }
}
