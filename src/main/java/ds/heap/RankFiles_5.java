package ds.heap;

import java.util.*;

/**
 * Given a list of [FileName, FileSize, [Collection]] - Collection is optional, i.e., a collection can have 1 or more files. Same file can be a part of more than 1 collection.
 * How would you design a system
 *
 * To calculate total size of files processed.
 * To calculate Top K collections based on size.
 * Example:
 * file1.txt(size: 100)
 * file2.txt(size: 200) in collection "collection1"
 * file3.txt(size: 200) in collection "collection1"
 * file4.txt(size: 300) in collection "collection2"
 * file5.txt(size: 100)
 * Output:
 *
 * Total size of files processed: 900
 * Top 2 collections:
 * - collection1 : 400
 * - collection2 : 300
 * */
public class RankFiles_5 {
    private static void solution(List<File> files, int k){
        int totalSize = 0;
        Map<String, Integer> collSizeMap = new HashMap<>();
        for(File file: files){
            totalSize += file.size;
            for(String collName : file.collections){
                collSizeMap.merge(collName, file.size, Integer::sum);
            }
        }
        PriorityQueue<Map.Entry<String, Integer>> collPq = new PriorityQueue<>((a, b) ->{
            if(a.getValue() == b.getValue())
                return a.getKey().compareTo(b.getKey());
            return a.getValue().compareTo(b.getValue());
        });

        for(Map.Entry<String, Integer> e: collSizeMap.entrySet()){
            if(collPq.size() < k){
                collPq.offer(e);
            }
            else {
                collPq.offer(e);
                if(collPq.size() > k)
                    collPq.poll();
            }
        }

        while (collPq.size() > k)
            collPq.poll();

        System.out.println("Total Size processed: "+totalSize);
        System.out.println("Top "+ k + " collections are: ");
        ArrayList<Map.Entry<String, Integer>> res = new ArrayList<>();
        while (!collPq.isEmpty())
            res.add(collPq.poll());
        Collections.reverse(res);
        res.forEach(System.out::println);
    }

    public static void main(String[] args) {
        List<File> files = new ArrayList<>();
        files.add(new File("file1.txt", 100, new ArrayList<>()));
        files.add(new File("file1.txt", 200, new ArrayList<>(Arrays.asList("collection1"))));
        files.add(new File("file1.txt", 200, new ArrayList<>(Arrays.asList("collection1"))));
        files.add(new File("file1.txt", 300, new ArrayList<>(Arrays.asList("collection2"))));
        files.add(new File("file1.txt", 50, new ArrayList<>(Arrays.asList("collection3"))));
        files.add(new File("file1.txt", 50, new ArrayList<>(Arrays.asList("collection3"))));
        files.add(new File("file1.txt", 300, new ArrayList<>(Arrays.asList("collection4"))));
        files.add(new File("file1.txt", 100, new ArrayList<>(Arrays.asList("collection4"))));


        solution(files, 3);
    }

}

class File{
    String name;
    int size;
    List<String> collections;

    public File(String name, int size, List<String> collections) {
        this.name = name;
        this.size = size;
        this.collections = collections;
    }
}
