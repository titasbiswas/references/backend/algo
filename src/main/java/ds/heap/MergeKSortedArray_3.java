package ds.heap;

import lombok.AllArgsConstructor;

import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

public class MergeKSortedArray_3 {
    public static void main(String[] args) {
        List<List<Integer>> arr = new ArrayList<>();
        arr.add(Arrays.asList(new Integer[]{10, 20}));
        arr.add(Arrays.asList(new Integer[]{5, 15}));
        arr.add(Arrays.asList(new Integer[]{2, 4, 11}));
        arr.add(Arrays.asList(new Integer[]{1, 3, 16, 19}));

        System.out.println(solution(arr));

    }
    private static List<Integer> solution(List<List<Integer>> arr){
        List<Integer> res = new ArrayList<>();
        PriorityQueue<Triplet> pq = new PriorityQueue<>();
        for (int i = 0; i < arr.size(); i++) {
            List<Integer> l = arr.get(i);
            pq.add(new Triplet(l.get(0), i, 0));
        }
        while (!pq.isEmpty()){
            Triplet t = pq.poll();
            res.add(t.value);
            int ap = t.arrayPos, ep = t.elementPos;
            if(ep+1 < arr.get(ap).size()){
                pq.add(new Triplet(arr.get(ap).get(ep+1), ap, ep+1));
            }
        }
        return res;
    }
}

@AllArgsConstructor
class Triplet implements Comparable<Triplet> {
    int value, arrayPos, elementPos;

    @Override
    public int compareTo(Triplet o) {
        return value <= o.value ? -1 : 1;
    }
}
