package ds.matrix;

import java.util.Arrays;

public class Rotate90Clock_5 {

    public static void main(String[] args) {
        int[][] matrix = {{1,2,3},{4,5,6},{7,8,9}};
        rotate(matrix);
        System.out.println(Arrays.deepToString(matrix));
    }
    public static void rotate(int[][] matrix) {
        transpose(matrix);
        revCol(matrix);
    }

    private static void transpose(int[][] m){
        int n = m.length;
        for(int i = 0; i<n; i++){
            for(int j= i; j< n; j++){
                int temp = m[i][j];
                m[i][j] = m[j][i];
                m[j][i] = temp;
            }
        }
    }

    private static void revCol(int[][] m){

        for(int i = 0; i<m.length; i++){
            int l = 0 , h = m[i].length-1;
            while(l<h){
                int temp = m[i][l];
                m[i][l] = m[i][h];
                m[i][h] = temp;
                l++; h--;
            }
        }
    }
}
