package ds.LinkedList;

public class SslDriver {
    public static void main(String[] args) {
        SinglyLinkedList<Integer> list1 = new SinglyLinkedList<>();
        for (int i = 1; i <=10; i++){
            list1.insertAtHead(i);
        }
        list1.printList();

        System.out.println("");
        SinglyLinkedList<Integer> list2 = new SinglyLinkedList<>();
        for (int i = 1; i <=10; i++){
            list2.insertAtEnd(i);
        }
        list2.printList();

        System.out.println("");
        list2.insertAfterData(88, 8);
        list2.printList();

        System.out.println("");
        System.out.println(list2.search(88));
        System.out.println(list2.search(98));

        list2.delete(88);
        list2.printList();
        System.out.println("");
        list2.delete(10);
        list2.printList();

    }
}
