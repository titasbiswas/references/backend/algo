package ds.LinkedList;

public class SinglyLinkedList<T> {

    class Node<T> {
        T data;
        Node next;
    }

    Node head;
    int size;

    public SinglyLinkedList() {
        head = null;
        size = 0;
    }

    public boolean isEmpty() {
        if (head == null) {
            return true;
        }
        return false;
    }

    public void printList() {
        if (isEmpty()) {
            System.out.println("List is empty");
        }

        Node temp = head;
        while (temp.next != null) {
            System.out.print(temp.data.toString() + "->");
            temp = temp.next;
        }
        System.out.print(temp.data.toString() + "->NULL");
    }

    public void insertAtHead(T data) {
        Node node = new Node();
        node.data = data;

        node.next = head;
        head = node;
        ++size;
    }

    public void insertAtEnd(T data) {
        if (isEmpty()) {
            insertAtHead(data);
            return;
        }
        Node node = new Node();
        node.data = data;
        node.next = null;

        Node last = head;
        while (last.next != null) {
            last = last.next;
        }
        last.next = node;
        ++size;
    }

    public void insertAfterData(T data, T prevNodeData) {
        Node node = new Node();
        node.data = data;

        Node currentNode = head;
        while (currentNode != null && !currentNode.data.equals(prevNodeData)) {
            currentNode = currentNode.next;
        }
        if (currentNode != null) {
            node.next = currentNode.next;
            currentNode.next = node;
            ++size;
        }
    }

    public boolean search(T data) {
        if (isEmpty()) {
            return false;
        }

        Node currentNode = head;
        while (currentNode != null) {
            if (currentNode.data.equals(data)) {
                return true;
            }
            currentNode = currentNode.next;
        }
        return false;
    }

    public void deleteAtHead() {
        if (isEmpty()) {
            return;
        }
        head = head.next;
        --size;
    }

    public void delete(T data) {
        if (isEmpty()) {
            return;
        }

        Node prevNode = null;
        Node currentNode = head;
        if (currentNode.data.equals(data)) {
            deleteAtHead();
        }
        while (currentNode != null) {
            if (currentNode.data.equals(data)) {
                prevNode.next = currentNode.next;
                --size;
                return;
            }
            prevNode = currentNode;
            currentNode = currentNode.next;
        }

    }
}
