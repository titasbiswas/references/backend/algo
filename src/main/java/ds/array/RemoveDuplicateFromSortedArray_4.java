package ds.array;

import java.util.Arrays;

public class RemoveDuplicateFromSortedArray_4 {
    public static void main(String[] args) {
        int[] arr = {5, 8, 8, 10};
        System.out.println(Arrays.toString(removeDup(arr)));
    }

    /**
     * Two pointer; If first pointer (i) finds a distinct element (arr[i] != arr[res-1])
     * place it to the position of a second pointer (res) and increment the second pointer as a
     * placeholder for next distinct element.
     * For first contiguous distinct elements it will replace it self*/
    private static int[] removeDup(int[] arr) {
        int res = 1; // Oth element is always district, start from 1th
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] != arr[res-1]) {
                arr[res] = arr[i];
                res++;
            }
        }
        return arr;
    }
}
