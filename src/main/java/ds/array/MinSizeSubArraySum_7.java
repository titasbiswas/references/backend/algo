package ds.array;

/**
 * Q:Given an array or positive integers, return the minimum length array that is greater or equals to the sum;
 * Two pointer at the left, traverse one pointer and add up the elements, when the sum become >= number,
 * negate the element in the second pointer and keep increasing the second pointer;
 * */
public class MinSizeSubArraySum_7 {

    public static void main(String[] args) {
        int arr[] = {2, 3, 1, 2, 4, 3, 1, 2, 3};
        int sum = 7;
        int res = calculateMinSubArrayLength(arr, sum); // return 2 -> {4, 3}
        System.out.println(res);
    }

    private static int calculateMinSubArrayLength(int[] arr, int sum) {
        int minLength = Integer.MAX_VALUE;
        int currSum = 0;// Calculate intermediate sum
        int left = 0;//Second pointer
        for (int i = 0; i < arr.length; i++) {
            currSum += arr[i];
            while(currSum >= sum){
                minLength = Math.min(minLength, i+1 - left); // Got a result, update it;
                currSum -= arr[left]; //Negate the value from the left
                left++; // increment left
            }
        }
        return minLength == Integer.MAX_VALUE ? 0: minLength;
    }
}
