package ds.array;

public class StockBuySell_8 {
    public static void main(String[] args) {

    }

    public int multipleTrading(int[] prices) {
        int profit = 0;
        for(int i = 1; i<prices.length;i++){
            if(prices[i] > prices[i-1]){
                profit += prices[i] - prices[i-1];
            }
        }
        return profit;
    }

    public int singleTrading(int[] prices) {
        int minTillNow = Integer.MAX_VALUE,  maxProfit = 0;
        for(int i = 0; i<prices.length;i++){
            minTillNow = Math.min(minTillNow, prices[i]);
            maxProfit = Math.max(maxProfit, prices[i] - minTillNow);
        }

        return maxProfit;
    }
}
