package ds.array;

public class MaxKsubArray_6 {
    /**
     * Sliding window
     */

    private static int sum(int[] arr, int k) {
        int windowSum = 0, max = 0;
        for (int i = 0; i < k; i++) {
            windowSum += arr[i];
        }
        max = windowSum;
        for(int i = k; i< arr.length; i++){
            windowSum += arr[i] - arr[i-k];
            max = Math.max(windowSum, max);
        }
        return max;
    }

    public static void main(String[] args) {
        int[] arr = {1,2,3,-2,4};//6
        System.out.println(sum(arr, 3));
    }
}
