package ds.array;

import java.util.HashMap;
import java.util.Map;

/**
 * Leetcode 904
 * You are visiting a farm that has a single row of fruit trees arranged from left to right. The trees are represented by an integer array fruits where fruits[i] is the type of fruit the ith tree produces.
 * You want to collect as much fruit as possible. However, the owner has some strict rules that you must follow:
 * You only have two baskets, and each basket can only hold a single type of fruit. There is no limit on the amount of fruit each basket can hold.
 * Starting from any tree of your choice, you must pick exactly one fruit from every tree (including the start tree) while moving to the right. The picked fruits must fit in one of your baskets.
 * Once you reach a tree with fruit that cannot fit in your baskets, you must stop.
 * Given the integer array fruits, return the maximum number of fruits you can pick.
 * Example 1:
 * <p>
 * Input: fruits = [1,2,1]
 * Output: 3
 * Explanation: We can pick from all 3 trees.
 * Example 2:
 * <p>
 * Input: fruits = [0,1,2,2]
 * Output: 3
 * Explanation: We can pick from trees [1,2,2].
 * If we had started at the first tree, we would only pick from trees [0,1].
 * Example 3:
 * <p>
 * Input: fruits = [1,2,3,2,2]
 * Output: 4
 * Explanation: We can pick from trees [2,3,2,2].
 * If we had started at the first tree, we would only pick from trees [1,2].
 * <p>
 * Soln: We need to get max length subarray with just two type of fruits (elements)
 * Sliding window with a hashmap to keep track of the type and count of fruits,
 */
public class FruitBasket_8 {
    public static void main(String[] args) {
        int[] fruits = {0, 1, 2, 2};
        int res = totalFruit(fruits);
        System.out.println(res);//3
    }

    public static int totalFruit(int[] fruits) {
        int maxFruit = 1;
        Map<Integer, Integer> countMap = new HashMap<>();
        int left = 0;
        for (int right = 0; right < fruits.length; right++) {
            int currFruitType = fruits[right];
            countMap.merge(currFruitType, 1, Integer::sum);
            // Adjust the window to keep the types to only 2, by removing elemnts from left
            while(countMap.size() > 2){
                countMap.merge(fruits[left], 1, (oldVal, val) -> oldVal-val>0?oldVal-val:null);
                left++;
            }
            maxFruit = Math.max(maxFruit, right-left+1);
        }
        return maxFruit;
    }
}
