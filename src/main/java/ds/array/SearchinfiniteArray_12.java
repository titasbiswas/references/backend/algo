package ds.array;

public class SearchinfiniteArray_12 {
    private int searchInfiteArray(int[] arr, int key){
        int start = 0;
        int end = 1;
        while(key > arr[end]){
            int temp = end;
            start = end +1;
            end = end + (end-start+1)*2;
            while (true){
                try {
                    int arbitVal = arr[end];
                    break;
                } catch (ArrayIndexOutOfBoundsException e) {
                    end--;
                }
            }
        }
        return binSearch(arr, start, end, key);
    }

    private int binSearch(int[] arr, int start, int end, int key) {
        if (end < start) {
            return -1;
        }
        int mid = (start + (end - start)) / 2;
        if (arr[mid] == key) {
            return mid;
        } else if (key < arr[mid]) {
            return binSearch(arr, start, mid - 1, key);
        } else {
            return binSearch(arr, mid + 1, end, key);
        }

    }
}
