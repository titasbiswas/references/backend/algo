package ds.array;

import java.util.ArrayList;
import java.util.List;

public class FillTheNumberRange_2 {

    public List<String> findMissingRanges(int[] nums, int lower, int upper) {

        int len = nums.length;
        List<String> res = new ArrayList<>();

        if(len == 0){
            res.add(String.valueOf(lower)+"-"+String.valueOf(upper));
            return res;
        }

        //the first case
        if(nums[0] > lower){ // 0, 1
            if(nums[0] - lower > 1){
                res.add(String.valueOf(lower) + "-" + String.valueOf(nums[0]));
            }else{
                res.add(String.valueOf(lower));
            }
        }
        for(int i = 1; i<len; i++){
            if(nums[i] - nums[i-1] > 2){
                res.add(String.valueOf(nums[i-1]+1) + "-" + String.valueOf(nums[i]-1));
            }else{
                res.add(String.valueOf(nums[i-1]+1));
            }
        }

        if(upper > nums[len -1]){
            if(upper - nums[len -1] > 1){
                res.add(String.valueOf(nums[len -1]+1) + "-" + String.valueOf(upper));
            }else{
                res.add(String.valueOf(upper));
            }
        }
        return res;

    }
}
