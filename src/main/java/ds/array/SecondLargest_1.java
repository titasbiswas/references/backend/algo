package ds.array;

public class SecondLargest_1 {
    public static void main(String[] args) {
        int[] arr = {5, 8, 20, 10};
        System.out.println(arr[secondLargest(arr)]);
    }

    /**
     * If a[i] > largest  : largest = second and a[i] = largest
     * If a[i]== largest : ignore
     * if a[i] < largest : compute for the all cases as in the comments
     */
    private static int secondLargest(int[] arr) {
        int res = -1, largest = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > arr[largest]) {
                res = largest;
                largest = i;
            }
            else if(arr[i] < arr[largest]){
                if(res == -1){ //there is no second largest so far, like 20, 20, 20, 12
                    res = i;
                }else if(arr[i]> arr[res]){ //there is no second largest so far, like 20, 8, 14, 12
                    res = i;
                }//ignore when arr[i]== arr[res]
                // The above two cases can be consolidated as if(res == -1 || arr[i]> arr[res])
            }

        }
        return res;
    }
}
