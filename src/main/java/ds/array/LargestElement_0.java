package ds.array;

public class LargestElement_0 {
    public static void main(String[] args) {
       int[] arr = {5, 8, 20 , 10};
        System.out.println(arr[largestElement(arr)]);
    }

    //returns the index of the largest element in the array
    private static int largestElement(int[] arr){
        int res = 0;
        for(int i=1;i< arr.length;i++){
            if(arr[i]>arr[res])
                res=i;
        }
        return res;
    }
}
