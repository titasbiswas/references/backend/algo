package ds.array;

import java.util.Arrays;
import java.util.HashSet;

public class TripletSum_11 {

    public static void main(String[] args) {
        int arr[] = {0, -1, 2, -3, 1};
        int sum = -2;
        int n = arr.length;
        findTriplets_hashing(arr, n, sum);
        findTriplets_twoPointer(arr, n, sum);
    }

    private static void findTriplets_hashing(int[] arr, int n, int sum) {
        for (int i = 0; i < n - 1; i++) {
            checkTwoSum(arr, i, sum);
        }
    }

    private static void checkTwoSum(int[] arr, int i, int sum) {
        HashSet<Integer> set = new HashSet<>();
        for (int j = i + 1; j < arr.length; j++) {
            int required = sum - arr[i] - arr[j];
            if (set.contains(required)) {
                System.out.println(required + " " + arr[i] + " " + arr[j]);
            } else {
                set.add(arr[j]);
            }
        }
    }

    private static void findTriplets_twoPointer(int[] arr, int n, int sum) {
        Arrays.sort(arr);
        for (int i = 0; i < n - 1; i++) {
            int l = i + 1, h = n - 1;
            while (l < h) {
                if (arr[i] + arr[l] + arr[h] == sum) {
                    System.out.println(arr[i] + " " + arr[l] + " " + arr[h]);
                    l++; h--;
                } else if (arr[i] + arr[l] + arr[h] < sum) {
                    l++;
                } else {
                    h--;
                }
            }
        }
    }
}
