package ds.array;

public class MaxSubArraySum_7 {
    /**
     * Q: Return the max sum of any sub array
     * Max @ i = Max of( MaxTill[i-1]+arr[i], arr[i])
     * max = Max of (max, max@i)
     */

    private static int sum(int[] arr) {
        int maxTillNow = arr[0], max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            maxTillNow = Math.max(maxTillNow + arr[i], arr[i]);
            max = Math.max(maxTillNow, max);
        }
        return max;
    }

    public static void main(String[] args) {
        int arr[] = {1, -2, 3, -1, 2}; //4 = {3, -1, 2}
        System.out.println(sum(arr));
    }
}
