package ds.array;
/**
 * Precompute
 * */
public class RainWaterTrapping_8 {
    private static int trappedWater(int[] arr) {
        int res = 0;
        int len = arr.length;
        int[] lmax = new int[len];
        int[] rmax = new int[len];
        lmax[0] = arr[0];
        rmax[len - 1] = arr[len - 1];
        for (int i = 1; i < len; i++) {
            lmax[i] = Math.max(arr[i], lmax[i-1]);
        }
        for (int i = len-2; i >= 0; i--) {
            rmax[i] = Math.max(arr[i], rmax[i+1]);
        }
        for (int i = 0; i < len; i++) {
            res += Math.min(lmax[i], rmax[i]) - arr[i];
        }

        return res;
    }

    public static void main(String[] args) {
        int[] arr = {5, 0, 6 , 2, 3};
        System.out.println(trappedWater(arr));
    }
}
