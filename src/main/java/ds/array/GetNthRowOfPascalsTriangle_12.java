package ds.array;

import java.util.ArrayList;
import java.util.List;

public class GetNthRowOfPascalsTriangle_12 {

    public static void main(String[] args) {
        System.out.println(getRow(50)); // [1, 3, 3, 1]
    }
    public static List<Integer> getRow(int rowIndex) {
        List<Integer> triangle = new ArrayList<>();
        triangle.add(1);
        for(int i= 1; i<= rowIndex; i++){
            List<Integer> cr = new ArrayList<>();
            cr.add(1);
            for(int j = 1; j<i; j++){
                cr.add(triangle.get(j-1)+triangle.get(j));
            }
            cr.add(1);
            triangle = cr;
        }

        return triangle;
    }

    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> triangle = new ArrayList<>();
        List<Integer> currRow = new ArrayList<>();
        currRow.add(1);
        triangle.add(currRow);
        for(int i= 1; i< numRows; i++){
            currRow = new ArrayList<>();
            currRow.add(1);
            for(int j = 1; j<i; j++){
                currRow.add(triangle.get(i-1).get(j-1)+triangle.get(i-1).get(j));
            }
            currRow.add(1);
            triangle.add(currRow);
        }

        return triangle;

    }

}
