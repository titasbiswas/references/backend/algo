package ds.array;

import java.util.HashMap;
import java.util.Map;

/** Leetcode 560
 * Given an array of integers nums and an integer k, return the total number of subarrays whose sum equals to k.
 *
 * A subarray is a contiguous non-empty sequence of elements within an array.
 * Soln:
 * Explanation:
 * The HashMap will store with the key being any particular sum, and the value being the number of times it has happened till the current iteration of the loop as we traverse the array from left to right.
 * For example:
 * - k = 26.
 * - If a sub-array sums up to k, then the sum at the end of this sub-array will be sumEnd = sumStart + k. That implies: sumStart = sumEnd - k.
 * - Suppose, at index 10, sum = 50, and the next 6 numbers are 8,-5,-3,10,15,1.
 * - At index 13, sum will be 50 again (the numbers from indexes 11 to 13 add up to 0).
 * - Then at index 16, sum = 76.
 * - Now, when we reach index 16, sum - k = 76 - 26 = 50. So, if this is the end index of a sub-array(s) which sums up to k, then before this, just before the start of the sub-array, the sum should be 50.
 * - As we found sum = 50 at two places before reaching index 16, we indeed have two sub-arrays which sum up to k (26): from indexes 14 to 16 and from indexes 11 to 16.
 * */
public class SubArraySumEqualsK_11 {

    public static void main(String[] args) {
        int[] nums = {1, 2, 3};
        int k = 3;
        int res = subarraySum(nums,k);
        System.out.println(res);

    }

    public static int subarraySum(int[] nums, int k) {
        int count = 0, curSum = 0;
        Map<Integer, Integer> sumOccuranceCountMap  = new HashMap<>();
        sumOccuranceCountMap.put(0, 1); // initiate the map with sum 0 seen once so fur
        for (int i = 0; i < nums.length; i++) {
            curSum += nums[i];
            int startSum = curSum - k;
            if(sumOccuranceCountMap.containsKey(startSum)){
                count += sumOccuranceCountMap.get(startSum);
            }
            sumOccuranceCountMap.merge(curSum, 1, Integer::sum);
        }

        return count;
    }
}
