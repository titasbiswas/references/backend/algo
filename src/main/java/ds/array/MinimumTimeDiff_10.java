package ds.array;

import java.util.Arrays;
import java.util.List;

/**
 * Given a list of 24-hour clock time points in "HH:MM" format, return the minimum minutes difference between any two time-points in the list.
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: timePoints = ["23:59","00:00"]
 * Output: 1
 * Example 2:
 * <p>
 * Input: timePoints = ["00:00","23:59","00:00"]
 * Output: 0
 *
 * Solution: As it's only 24*60 mins, create a boolean array 'visited' to accommodate all time stamps.
 * Calculate the min difference between two adjacent 'visited[true]' i.e (curr_visited - prev_visited),
 * keep a track of prev visited
 * Optimization: Onlty traverse from fist visited to last visited
 */

public class MinimumTimeDiff_10 {
    private static int findMinDifference(List<String> timePoints) {
        boolean[] visited = new boolean[24 * 60];
        for (String time : timePoints) {
            int h = Integer.parseInt(time.substring(0, 2));
            int m = Integer.parseInt(time.substring(3));
            if (visited[h * 60 + m]) return 0; // if visited before return 0
            visited[h * 60 + m] = true; // mark as visited
        }

        int prev = 0, min = Integer.MAX_VALUE;
        int first = 0, last = 0;
        int idx = visited.length - 1; // find last first, so idx points to the end

        // find the last true from the back, or the last number(max minutes)
        while (!visited[idx])
            idx--;
        last = idx;

        // find the first true, or the first number(min minutes)
        idx = 0;
        while (!visited[idx])
            idx++;
        first = idx;

        prev = first;
        for (int i = first + 1; i <= last; i++) {
            if (visited[i]) {
                min = Math.min(min, i - prev);
                prev = i;
            }
        }

        // since every two consectutive are compared, we also need to
        // compare the diff between first one and the last
        min = Math.min(min, (24 * 60 - last + first));
        return min;
    }

    public static void main(String[] args) {
        String[] arr = {"00:00", "23:59"};
        System.out.println(findMinDifference(Arrays.asList(arr)));
    }
}
