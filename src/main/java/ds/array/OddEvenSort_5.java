package ds.array;

import java.util.Arrays;

/***
 * Get the even numbers first
 */

public class OddEvenSort_5 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        int[] arr2 = Arrays.copyOf(arr, arr.length);

        paritySort(arr);
        System.out.println(Arrays.toString(arr));

        System.out.println(Arrays.toString(paritySort2(arr2)));

    }

    private static void paritySort(int[] arr) {
        int i= 0, j = arr.length -1;
        while(i<j){
            if(arr[i]%2 == 1 && arr[j]%2==0){
                ArrayUtils.swap(arr, i, j);
                i++;
                j--;
            }
            if(arr[i]%2 == 0){
                i++;
            }
            if(arr[j]%2==1){
                j--;
            }
        }
    }
//Odd first
    private static int[] paritySort2(int[] arr) {
        int j = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]%2 != 0) {
                ArrayUtils.swap(arr, j, i);
                j++;
            }

        }
        return arr;
    }
}
