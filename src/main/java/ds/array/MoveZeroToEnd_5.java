package ds.array;

import java.util.Arrays;

public class MoveZeroToEnd_5 {
    public static void main(String[] args) {
        int[] arr = {5, 8, 0, 0, 10, 0, 20};
        int[] arr2 = Arrays.copyOf(arr, arr.length);
        System.out.println(Arrays.toString(zeroToEnd(arr)));
        System.out.println(Arrays.toString(zeroToStart(arr2)));

    }

    /**
     * Two pointer; Keep on counting the nonzero element, the index of the first zero would be the count
     * So we can swap  it with the next non zero element and move the counter to next
     * First non zero elements swap with itself
     * */

    private static int[] zeroToEnd(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0) {
                ArrayUtils.swap(arr, count, i);
                count++;
            }

        }
        return arr;
    }

    private static int[] zeroToStart(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                ArrayUtils.swap(arr, count, i);
                count++;
            }
        }
        return arr;
    }
}
