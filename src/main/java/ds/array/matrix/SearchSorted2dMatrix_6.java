package ds.array.matrix;

/**
 * Leetcode 74
 * Write an efficient algorithm that searches for a value target in an m x n integer matrix matrix. This matrix has the following properties:
 * Integers in each row are sorted from left to right.
 * The first integer of each row is greater than the last integer of the previous row.
 * <p>
 * Soln: Bin search, consider it as single array, calculate mid element as mat[mid/columns][mid%columns]
 */
public class SearchSorted2dMatrix_6 {
    public static void main(String[] args) {
        int[][] mat = {{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}};
        int target = 3;

        var res = searchMatrix(mat, 3);
        System.out.println(res);
    }

    public static boolean searchMatrix(int[][] matrix, int target) {
        if (matrix.length == 0) return false;
        int noRows = matrix.length;
        int noCols = matrix[0].length;
        int left = 0;
        int right = noRows * noCols - 1;

        while (right >= left){
            int mid = left+(right-left)/2;
            int midElement = matrix[mid/noCols][mid%noCols];
            if(midElement == target){
                return true;
            } else if (target > midElement) {
                left = mid+1;
            }
            else {
                right = mid -1;
            }
        }

        return false;
    }
}

