package ds.array.matrix;

import java.util.Arrays;

public class SpiralMatrixFill_5 {
    public static void main(String[] args) {
        int n = 3;
        int[][] res = fillMatrix(n);
        System.out.println(Arrays.deepToString(res));
    }

    private static int[][] fillMatrix(int n) {
        int[][] res = new int[n][n];
        int rowBegin = 0 , rowEnd = n-1, colBegin =0, colEnd = n-1;
        int filler = 1;
        while(rowBegin<= rowEnd && colBegin<=colEnd){

            for (int i = colBegin; i <= colEnd; i++) {
                res[rowBegin][i] = filler;
                filler++;
            }
            rowBegin++;
            for (int i = rowBegin; i <= rowEnd; i++) {
                res[i][colEnd] = filler;
                filler++;
            }
            colEnd--;
            if(colBegin<= colEnd){// as we changed the values within the outer while loop
                for (int i = colEnd; i >= colBegin; i--) {
                    res[rowEnd][i] = filler;
                    filler++;
                }
                rowEnd--;
            }
            if(rowBegin<= rowEnd){
                for (int i = rowEnd; i >= rowBegin; i--) {
                    res[i][colBegin] = filler;
                    filler++;
                }
                colBegin++;
            }
        }
        return res;
    }
}
