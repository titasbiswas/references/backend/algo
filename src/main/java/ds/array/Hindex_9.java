package ds.array;

import java.util.Arrays;

public class Hindex_9 {
    public static void main(String[] args) {
        int[] arr = {3, 0, 6, 1, 5};//{1, 3, 1};//
        System.out.println(solution_eff(arr));
    }

    private static int solution(int[] arr) {
        Arrays.sort(arr); // {0, 1, 3, 5, 6}
        int n = arr.length, res = -1;
        for (int i = 0; i < n - 1; i++) {
            if (arr[n - 1 - i] <= i) {
                res = i;
                break;
            }
        }
        return res;
    }

    private static int solution_eff(int[] arr) {
        int len = arr.length;
        int[] counts = new int[len + 1];
        for (int c : arr) {
            if (c > len) {
                counts[len]++;
            } else {
                counts[c]++;
            }
        }
        System.out.println(Arrays.toString(counts));
        int totalCit = 0;
        for (int i = len; i >= 0; i--) {
            totalCit += counts[i];
            if (totalCit >= i) {
                return i;
            }
        }
        return 0;
    }
}
