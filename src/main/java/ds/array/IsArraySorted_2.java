package ds.array;

public class IsArraySorted_2 {
    public static void main(String[] args) {
        int[] arr = {5, 8, 20, 10};
        System.out.println(isSorted(arr));
        int[] arr2 = {5, 8, 10, 10, 15};
        System.out.println(isSorted(arr2));
    }

    private static boolean isSorted(int[] arr) {
        for (int i = 1; i < arr.length ; i++) {
            if(arr[i]<arr[i-1]) // Handles the equal case as well
                return false;
        }
        return true;
    }

}
