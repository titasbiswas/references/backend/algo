package ds.array;

import java.util.Arrays;

public class ReverseAnArray_3 {
    public static void main(String[] args) {
        int[] arr = {5, 8, 20, 10};
        System.out.println(Arrays.toString(reverseArray(arr)));
        int[] arr2 = {5, 7, 9};
        System.out.println(Arrays.toString(reverseArray(arr2)));
    }

    private static int[] reverseArray(int[] arr) {
        int low = 0, high = arr.length-1, temp;
        while(high>low){
            temp = arr[low];
            arr[low++] = arr[high];
            arr[high--] = temp;
        }
        return arr;
    }

}
