package ds.tree;

import java.util.Stack;

public class FlattenTreeToList_6 {

    public static void main(String[] args) {
        TreeNode root = DriverClass.stringToTreeNode("[1,2,5,3,4,null,6]");
        DriverClass.printLevelOrder(root);

        flattenTree(root);
        DriverClass.printLevelOrder(root);
    }

    private static void flattenTree(TreeNode root) {
        if(root == null){
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while(!stack.isEmpty()){
            TreeNode curr = stack.pop();
            if(curr.right != null){
                stack.push(curr.right);
            }
            if(curr.left != null){
                stack.push(curr.left);
            }
            if(!stack.isEmpty()){ // get the left node and put it to the right node but dont remove it for its next childs;
                curr.right =  stack.peek();
            }
            curr.left = null;
        }

    }
}
