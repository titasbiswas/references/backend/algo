package ds.tree;

import ds.tree.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LevelOrderTraversal {



    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        Queue<TreeNode> q = new LinkedList<>();
        List<Integer> levelList = null;
        if (root != null)
            q.add(root);
        while (!q.isEmpty()) {

            int qSize = q.size();
            levelList = new ArrayList<>();
            TreeNode cur;
            while (qSize > 0) {
                 cur = q.remove();
                levelList.add(cur.val);
                if(cur.left != null){
                    q.add(cur.left);
                }
                if(cur.right != null){
                    q.add(cur.right);
                }
                qSize--;
            }
            result.add(levelList);
        }

        return result;
    }

    public static void main(String[] args) {
        TreeNode root = DriverClass.stringToTreeNode("[3,9,20,null,null,15,7]");
        System.out.println(levelOrder(root)); //[[3],[9,20],[15,7]]
    }

}
