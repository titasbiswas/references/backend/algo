package ds.tree;

public class MaxDepthOfBinaryTree {
    static int maxDepth = 0;

    static void maxDepth_1(TreeNode node, int depth) {
        if (node == null) {
            return;
        }
        if (node.left == null && node.right == null) { // depth at leaf node
            maxDepth = Math.max(depth, maxDepth);
        }
        maxDepth_1(node.left, depth + 1);
        maxDepth_1(node.right, depth + 1);
    }
    public static void main(String[] args) {
        TreeNode root = DriverClass.stringToTreeNode("[3,9,20,null,null,15,7]");
        maxDepth_1(root, 1);
        System.out.println(maxDepth); //3
        System.out.println(maxDepth_2(root));
    }

    static int maxDepth_2(TreeNode node){
        if (node == null) {
            return 0;
        }else {
            int leftDepth = maxDepth_2(node.left);
            int rightDepth =maxDepth_2(node.right);
            return Math.max(leftDepth, rightDepth)+1;
        }
    }
}
