package ds.tree;

import java.util.*;

public class MaxofEachLevel_5 {
    public static void main(String[] args) {
        TreeNode root = DriverClass.stringToTreeNode("[3,9,20,null,null,15,7]");
        //[[3],[9,20],[15,7]]
        System.out.println(getMaxes(root));
    }

    private static List<Integer> getMaxes(TreeNode root) {
        if (root == null) {
            return Collections.emptyList();
        }
        List<Integer> res = new ArrayList<>();
        Queue<TreeNode> q = new LinkedList<>();
        q.add(root);
        int max;
        while (!q.isEmpty()) {
            max = Integer.MIN_VALUE;
            int qSize = q.size();
            TreeNode curNode;
            for (int i = 0; i < qSize; i++) {
                curNode = q.poll();
                max = Math.max(max, curNode.val);
                if (curNode.left != null) {
                    q.add(curNode.left);
                }
                if (curNode.right != null) {
                    q.add(curNode.right);
                }
            }
            res.add(max);
            max = Integer.MIN_VALUE;
        }
        return res;
    }
}
