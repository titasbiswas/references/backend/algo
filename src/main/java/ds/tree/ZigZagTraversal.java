package ds.tree;

import java.util.*;

public class ZigZagTraversal {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        Queue<TreeNode> q = new LinkedList<>();
        List<Integer> levelList = null;
        if (root == null)
            return result;
        int qSize = 0;
        boolean rf = false;
        q.add(root);
        while (!q.isEmpty()) {
            qSize = q.size();
            levelList = new ArrayList<>();

            while (qSize > 0) {
                TreeNode n = q.poll();
                levelList.add(n.val);


                if (n.left != null)
                    q.add(n.left);
                if (n.right != null)
                    q.add(n.right);

                qSize--;

            }
            if (rf) {
                Collections.reverse(levelList);
            }
            rf = !rf;
            result.add(levelList);
        }
        return result;
    }
}
