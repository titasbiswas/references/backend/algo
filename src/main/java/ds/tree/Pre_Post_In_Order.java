package ds.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Pre_Post_In_Order {
    public static void main(String[] args) {
        TreeNode root = DriverClass.stringToTreeNode("[1,null,2,3]");
        preOrder(root);
        System.out.println(preOrderIterative(root));
        System.out.println("");
        inOrder(root);
        System.out.println(inOrderIterative(root));
        System.out.println("");
        postOrder(root);
        System.out.println(postOrderIterative(root));

    }

    static void preOrder(TreeNode node){
        if(node == null){
            return;
        }
        System.out.print(node.val);
        preOrder(node.left);
        preOrder(node.right);
    }

    static void inOrder(TreeNode node){
        if(node == null){
            return;
        }
        preOrder(node.left);
        System.out.print(node.val);
        preOrder(node.right);
    }

    static void postOrder(TreeNode node){
        if(node == null){
            return;
        }
        preOrder(node.left);
        preOrder(node.right);
        System.out.print(node.val);

    }
    static List<Integer> preOrderIterative(TreeNode node){
        List<Integer> res = new ArrayList<>();
        Stack<TreeNode> visited = new Stack<>();
        if(node != null){
            visited.push(node);
        }
        TreeNode curr;
        while(!visited.empty()){
           curr = visited.pop();
           res.add(curr.val);
           if(curr.right != null){
               visited.push(curr.right);
           }
            if(curr.left != null){
                visited.push(curr.left);
            }
        }
        return res;
    }

    static List<Integer> inOrderIterative(TreeNode node){
        List<Integer> res = new ArrayList<>();
        Stack<TreeNode> visited = new Stack<>();
        if(node != null){
            visited.push(node);
        }
        TreeNode curr;
        while(!visited.empty()){
            curr = visited.pop();

            if(curr.right != null){
                visited.push(curr.right);
            }
            if(curr.left != null){
                visited.push(curr.left);
            }
            res.add(curr.val);
        }
        return res;
    }
    static List<Integer> postOrderIterative(TreeNode node){
        List<Integer> res = new ArrayList<>();
        Stack<TreeNode> visited = new Stack<>();
        if(node != null){
            visited.push(node);
        }
        TreeNode curr;
        while(!visited.empty()){
            curr = visited.pop();

            if(curr.right != null){
                visited.push(curr.right);
            }
            res.add(curr.val);
            if(curr.left != null){
                visited.push(curr.left);
            }

        }
        return res;
    }
}
