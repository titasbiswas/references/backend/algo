package ds.tree;

/**
 * We can call it symmetric
 * - if two root nodes values are same
 * - left tree and right sub tree is symmetric
 */
public class InBinTreeSymmetric {
    static boolean isSymmetric(TreeNode node) {
        return check(node, node);
    }

    private static boolean check(TreeNode left, TreeNode right) {
        if (left == null && right == null) return true;
        if (left == null || right == null) return false;
        return (right.val == left.val) &&
                check(right.left, left.right) &&
                check(right.right, left.left);
    }

    public static void main(String[] args) {
        TreeNode root = DriverClass.stringToTreeNode("[1,2,2,3,4,4,3]");
        System.out.println(isSymmetric(root));//true
        TreeNode root2 = DriverClass.stringToTreeNode("[1,2,2,null,3,null,3]");
        System.out.println(isSymmetric(root2)); // false
    }
}
