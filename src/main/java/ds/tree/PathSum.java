package ds.tree;

public class PathSum {

    //int currSum = 0;
    boolean answer = false;

    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null)
            return false;

        checkSum(root, sum, 0);
        return answer;
    }

    private void checkSum(TreeNode node, int sum, int currSum) {
        if (node == null)
            return;

        currSum += node.val;
        if (node.left == null && node.right == null && currSum == sum)
            answer = true;

        checkSum(node.left, sum, currSum);
        checkSum(node.right, sum, currSum);


    }
}
