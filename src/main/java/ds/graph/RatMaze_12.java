package ds.graph;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class RatMaze_12 {

    static int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

    public static void main(String[] args) {
        int[][] maze = {
                {1, 1, 1, 1, 1},
                {0, 0, 1, 0, 1},
                {1, 1, 1, 1, 1},
                {1, 0, 0, 0, 0},
                {1, 1, 1, 1, 1},
        };
        int n = maze.length;

        Point init = new Point(0, 0, 0);
        Point dest = new Point(n - 1, n - 1);

        int step = freeTheRatBFS(maze, n, init, dest);
        System.out.println(step);

        step = freeTheRatDFS(maze, n, init, dest, 0, new HashSet<Point>());
        System.out.println(step);

    }

    private static int freeTheRatBFS(int[][] maze, int n, Point init, Point dest) {
        Queue<Point> q = new LinkedList<>();
        Set<Point> visited = new HashSet<>();
        q.offer(init);
        while (!q.isEmpty()) {
            Point curr = q.poll();
            visited.add(curr);
            if (curr.equals(dest)) {
                return curr.dist;
            }
            for (int[] dir : dirs) {
                Point temp = new Point(curr.sr + dir[0], curr.sc + dir[1], curr.dist + 1);
                if (isValid(temp, maze, n) && !visited.contains(temp)) {
                    q.offer(temp);
                }
            }
        }

        return -1;
    }

    //TODO Wrong solution, need to correct it;
    private static int freeTheRatDFS(int[][] maze, int n, Point curr, Point dest, int dist, Set<Point> visited) {
        if (curr.equals(dest)) {
            return dist;
        }
        if (!isValid(curr, maze, n) || visited.contains(curr)) {
            return dist;
        }

        visited.add(curr);

        for (int[] dir : dirs) {
            Point temp = new Point(curr.sr + dir[0], curr.sc + dir[1]);
            return freeTheRatDFS(maze, n, temp, dest, dist + 1, visited);

        }
        return -1;
    }

    private static boolean isValid(Point temp, int[][] maze, int n) {
        if (temp.sr >= 0 && temp.sr < n && temp.sc >= 0 && temp.sc < n && maze[temp.sr][temp.sc] == 1) {
            return true;
        }
        return false;
    }

    @AllArgsConstructor
    @NoArgsConstructor
    static class Point {
        int sr, sc, dist;

        public Point(int sr, int sc) {
            this.sr = sr;
            this.sc = sc;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Point) {
                return ((Point) o).sc == this.sc && ((Point) o).sr == this.sr;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return sc * 37 + sr * 17;
        }

    }
}


