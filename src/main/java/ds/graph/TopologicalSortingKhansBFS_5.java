package ds.graph;

import javax.print.attribute.IntegerSyntax;
import java.util.*;

public class TopologicalSortingKhansBFS_5 {
    public static void main(String[] args) {
        DirectedGraph graph = new DirectedGraph(5);
        graph.addEdge(0, 2);
        graph.addEdge(0, 3);
        graph.addEdge(2, 3);
        graph.addEdge(1, 3);
        graph.addEdge(1, 4);
        List<List<Integer>> res = printTopo(graph);
        for (List<Integer> level : res) {
            System.out.println(level);
        }
    }

    private static List<List<Integer>> printTopo(DirectedGraph graph) {
        List<List<Integer>> result = new ArrayList<>();
        //Create in direction degree map for every vertex
        Map<Integer, Integer> degreeMap = new HashMap<>();
        for (int i = 0; i < graph.adj.size(); i++) {
            degreeMap.putIfAbsent(i, 0);
            List<Integer> u = graph.adj.get(i);
            for (Integer v : u) {
                degreeMap.merge(v, 1, Integer::sum);
            }
        }
        Queue<Integer> queue = new LinkedList<>();
        //Queue all vertex with 0 degree
        for (int i = 0; i < graph.adj.size(); i++) {
            if (degreeMap.get(i) == 0) {
                queue.offer(i);
            }
        }
        List<Integer> level;
        while (!queue.isEmpty()) {
            int size = queue.size();
            level = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                Integer currVertex = queue.poll();
                level.add(currVertex);
                for (Integer v : graph.adj.get(currVertex)) {
                    int count = degreeMap.get(v) - 1;
                    if (count == 0) {
                        queue.add(v);
                    }
                    degreeMap.put(v, count);
                }
            }
            result.add(level);
        }
        return result;
    }
}
