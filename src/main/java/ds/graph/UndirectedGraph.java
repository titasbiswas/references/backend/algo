package ds.graph;

import java.util.ArrayList;

public class UndirectedGraph {
    int vetices;
    ArrayList<ArrayList<Integer>> adj;

    public UndirectedGraph(int size) {
        this.vetices = size;
        adj = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            adj.add(new ArrayList<>());
        }
    }

    public void addEdge(int source, int destination) {

        adj.get(source).add(destination);
        adj.get(destination).add(source);
    }
}

