package ds.graph;

import java.util.List;

public class SimpleDFS {
    static boolean[] visited;
    public static void dfs(int v, UndirectedGraph undirectedGraph){
        visited[v] = true;
        System.out.println("visited: "+v);
        List<Integer> vertices = undirectedGraph.adj.get(v);
        for(Integer i : vertices){
            if(!visited[i]){
                visited[i] = true;
                dfs(i, undirectedGraph);
            }
        }
    }

    public static void main(String[] args) {
        UndirectedGraph g = new UndirectedGraph(4);

        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 3);
        g.addEdge(3, 3);

        visited = new boolean[g.vetices];
        SimpleDFS simpleDFS = new SimpleDFS();
        simpleDFS.dfs(2, g);
    }
}
