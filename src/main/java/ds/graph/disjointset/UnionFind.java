package ds.graph.disjointset;

public class UnionFind {
    int vertices;
    int[] root;
    int[] rank;
    int setCount;

    public UnionFind(int vertices) {
        this.vertices = vertices;
        root = new int[vertices];
        rank = new int[vertices];
        //all vertices are set itself as there are no unions
        setCount = vertices;
        for (int i = 0; i < vertices; i++) {
            root[i] = i;
            rank[i] = 1;
        }
    }

    public int find(int i){
        if(root[i] == i){
            return i;
        }
        return root[i] = find(root[i]);
    }

    public void union(int x , int y){
        int rootX = find(x);
        int rootY = find(y);
        if(rootX != rootY){
            if(rank[rootX]>rank[rootY]){
                root[rootY] = rootX;
            }else if(rank[rootX]<rank[rootY]){
                root[rootX] = rootY;
            }
            else {
                root[rootY] = rootX;
                //rootX now have a now have one more branch hence it's height is increased by one
                ++rank[rootX];
            }
            //On each union set count decreases
            --setCount;
        }
    }
}
