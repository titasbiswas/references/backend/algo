package ds.graph.disjointset;

public class FindProvinces {
    public static int findCircleNum(int[][] isConnected) {
        UnionFind djs = new UnionFind(isConnected.length);
        for (int i = 0; i < isConnected.length; i++) {
            for (int j = 0; j < isConnected.length; j++) {
                if (isConnected[i][j] == 1) {
                    djs.union(i, j);
                }
            }
        }
        return djs.setCount;
    }

    public static void main(String[] args) {
        int[][] isConnected = {{1, 1, 0}, {1, 1, 0}, {0, 0, 1}};
        System.out.println(findCircleNum(isConnected));
    }
}
