package ds.graph;

import java.util.LinkedList;
import java.util.Queue;

public class SimpleBFS {

    public static void bfs(int v, UndirectedGraph undirectedGraph){
        boolean[] visited = new boolean[undirectedGraph.vetices];
        Queue<Integer> tq = new LinkedList<>();

        visited[v]=true;
        tq.add(v);

        while(!tq.isEmpty()) {
            int vv = tq.poll();
            System.out.println("visited: "+vv);
            for(int e: undirectedGraph.adj.get(vv)){
                if(!visited[e]){
                    visited[e]=true;
                    tq.add(e);
                }
            }
        }
    }

    public static void main(String[] args) {
        UndirectedGraph g = new UndirectedGraph(7);

        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 3);
        g.addEdge(3, 3);
        g.addEdge(3, 4);
        g.addEdge(3, 5);
        g.addEdge(3, 6);

        SimpleBFS SimpleBFS = new SimpleBFS();
        SimpleBFS.bfs(0, g);
    }
}
