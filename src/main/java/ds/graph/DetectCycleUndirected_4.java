package ds.graph;

import java.util.HashSet;
import java.util.Set;

public class DetectCycleUndirected_4 {
    public static void main(String[] args) {
        UndirectedGraph undirectedGraph = new UndirectedGraph(6);
        undirectedGraph.addEdge(0,1);
        undirectedGraph.addEdge(1,2);
        undirectedGraph.addEdge(2,4);
        undirectedGraph.addEdge(4,5);
        undirectedGraph.addEdge(1,3); // This edge connects it, can be commented to check for non cycle;
        undirectedGraph.addEdge(2,3);
        boolean res = detectCycleDFS(undirectedGraph);
        System.out.println(res);
    }

    private static boolean detectCycleDFS(UndirectedGraph undirectedGraph) {
        Set<Integer> visited = new HashSet<>();
        for (int i = 0; i < undirectedGraph.vetices; i++) {
            if(!visited.contains(i)){
               if(detectCycleDFSRec(undirectedGraph, visited, i, -1) == true){
                   return true;
               }
            }
        }
        return false;
    }

    private static boolean detectCycleDFSRec(UndirectedGraph undirectedGraph, Set<Integer> visited, int curr, int parent) {
        visited.add(curr);
        for(int i: undirectedGraph.adj.get(curr)){
            if(!visited.contains(i)){
                if(detectCycleDFSRec(undirectedGraph, visited, i, curr) == true){
                    return true;
                }
            } else if (i != parent) {// Main check, if the vertics has already been visited but not its parent
                return true;
            }
        }
        return false;
    }


}
