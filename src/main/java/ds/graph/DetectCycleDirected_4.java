package ds.graph;

import java.awt.*;
import java.util.*;
import java.util.List;

public class DetectCycleDirected_4 {
    public static void main(String[] args) {
        DirectedGraph graph = new DirectedGraph(6);
        graph.addEdge(0,1);
        graph.addEdge(2,1);
        graph.addEdge(2,3);
        graph.addEdge(3,4);
        graph.addEdge(4,5);
        graph.addEdge(5,3);// This edge connects it, can be commented to check for non cycle;
        boolean res = detectCycleDFS(graph);
        System.out.println(res);
        res = detectCycleKahns(graph);
        System.out.println(res);
    }

    /**
     * The idea is to keep counting the vertices explored when poping out from the queue.
     * If count < no of vertices, then all vertices has not been explored because of a cycle
     * */
    private static boolean detectCycleKahns(DirectedGraph graph) {
        //calculate in degrees for each vertex
        Map<Integer, Integer> degreeMap = new HashMap<>();
        for (int i = 0; i < graph.adj.size(); i++) {
            degreeMap.putIfAbsent(i, 0);
            List<Integer> u = graph.adj.get(i);
            for (Integer v : u) {
                degreeMap.merge(v, 1, Integer::sum);
            }
        }

        //explore the vertices in a DFS manner
        Queue<Integer> queue = new LinkedList<>();
        int exploredCount = 0;
        //Queue all vertex with 0 degree
        for (int i = 0; i < graph.adj.size(); i++) {
            if (degreeMap.get(i) == 0) {
                queue.offer(i);
            }
        }
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                Integer currVertex = queue.poll();
                exploredCount++;
                for (Integer v : graph.adj.get(currVertex)) {
                    int count = degreeMap.get(v) - 1;
                    if (count == 0) {
                        queue.add(v);
                    }
                    degreeMap.put(v, count);
                }
            }
        }
        //compute whether all the vertices has been explored and return result
        return (exploredCount != graph.adj.size());
    }

    private static boolean detectCycleDFS(DirectedGraph graph) {
        Set<Integer> visited = new HashSet<>();
        Set<Integer> recStack = new HashSet<>();
        for (int i = 0; i < graph.vetices; i++) {
            if(!visited.contains(i)){
               if(detectCycleDFSRec(graph, visited, recStack, i, -1) == true){
                   return true;
               }
            }
        }
        return false;
    }

    private static boolean detectCycleDFSRec(DirectedGraph graph, Set<Integer> visited, Set<Integer> recStack, int curr, int parent) {
        visited.add(curr);
        recStack.add(curr);
        for(int i: graph.adj.get(curr)){
            if(!visited.contains(i)){
                if(detectCycleDFSRec(graph, visited, recStack, i, curr) == true){
                    return true;
                }
            } else if (recStack.contains(i)) {// Main check, if the vertex is present in the recursion stack, then there is a cycle
                return true;
            }
        }
        recStack.clear(); // At this point it is out of the stack, reset the recstack
        return false;
    }


}
