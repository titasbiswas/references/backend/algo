package ds.graph;

import java.util.Arrays;

public class FloodFill_10 {

    public static void main(String[] args) {
        int[][] image = {
                {1, 1, 1},
                {1, 1, 0},
                {1, 0, 1}};
        int sr = 1, sc = 1, color = 2;

        System.out.println(Arrays.deepToString(floodFill(image, sr, sc, color)));
    }

    public static int[][] floodFill(int[][] image, int sr, int sc, int color) {
        int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        if (image[sr][sc] != color)
            fill(image, sr, sc, image[sr][sc], color, dirs);
        return image;
    }

    public static void fill(int[][] image, int sr, int sc, int initialColor, int color, int[][] dirs) {
        if (sr >= image.length || sr < 0 || sc >= image[0].length || sc < 0) {
            return;
        }
        if (image[sr][sc] != initialColor) {
            return;
        }
        image[sr][sc] = color;
        for (int[] dir : dirs) {
            fill(image, sr + dir[0], sc + dir[1], initialColor, color, dirs);
        }
    }
}
