package ds.graph;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.*;

/**
 * Count the number of fresh water lakes in the islands. Islands are located in infinite ocean represented by an image.
 * 'X' denotes land and '.' denote water.
 * Input: the image as matrix, co-ordinate of any point of an island.
 * */
public class CountFreshWaterLakes_11 {

    public static void main(String[] args) {
        int[][] image = {
                {'.', 'X', '.', '.', '.', '.','.','X', 'X', 'X', '.', '.'},
                {'.', '.', '.', '.', '.', '.','.','X', '.', 'X', '.', '.'},
                {'.', '.', 'X', 'X', 'X', 'X','.','X', 'X', 'X', '.', '.'},
                {'.', '.', 'X', '.', 'X', 'X','.','X', '.', 'X', '.', '.'},
                {'.', '.', 'X', '.', 'X', 'X','.','X', '.', 'X', '.', '.'},
                {'.', '.', 'X', 'X', 'X', 'X','.','X', 'X', 'X', '.', '.'},
                {'.', '.', '.', '.', '.', '.','.','X', '.', 'X', '.', '.'},
                {'.', '.', '.', '.', '.', '.','.','X', 'X', 'X', '.', '.'},
                {'.', '.', '.', '.', '.', '.','.','.', '.', '.', '.', '.'},
                {'.', '.', '.', '.', '.', '.','.','.', '.', '.', '.', '.'}
        };

        int count = countFreshWaterLakes(image, new int[]{2,7 });
        System.out.println("No of islands: "+count);
    }


    private static int countFreshWaterLakes(int[][] image, int[] landCoord){
        int count = 0;
        int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        Point root = new Point(landCoord[1], landCoord[0]);
        Set<Point> island = new HashSet<>();
        buildIsLand(image, island, root, dirs);
        List<Point> isLandPoints = new ArrayList<>(island);
        Comparator<Point> cmp = Comparator.comparing(Point::getX).thenComparing(Point::getY);
        isLandPoints.sort(cmp);
        //get first and last from sorted > search within bounday for '.' > Start dfs/bfs mark '.' to 'x'
        Point start  = isLandPoints.get(0);
        Point end = isLandPoints.get(isLandPoints.size()-1);
        //Traverse the island
        for (int i = start.y  ; i <= end.y; i++) {
            for (int j = start.x; j <= end.x ; j++) {
                if(image[i][j] == '.'){
                    count++;
                    coverTheLake(image, i, j, dirs);
                }
            }
        }

        return count;
    }

    private static void coverTheLake(int[][] image, int sr, int sc, int[][] dirs) {
        if(image[sr][sc] != '.'){
            return;
        }
        image[sr][sc] = 'X';
        for (int[] dir : dirs) {
            coverTheLake(image, sr + dir[0], sc + dir[1], dirs);
        }
    }

    private static void buildIsLand(int[][] image, Set<Point> island, Point root, int[][] dirs) {
        if(root.x < 0 || root.x >= image[0].length || root.y < 0 || root.y >= image.length){
            return;
        }
        if(image[root.y][root.x] == '.' || island.contains(root)){
            return;
        }
        island.add(root);
        for (int[] dir : dirs) {
            buildIsLand(image, island, new Point(root.x + dir[0], root.y + dir[1]), dirs);
        }
    }
}

@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Data
class Point{
    int x, y;
}
