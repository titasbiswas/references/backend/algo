package ds.stack;

import java.util.ArrayDeque;
import java.util.Deque;

public class PreviousGreaterElement_1 {
    public static void main(String[] args) {
        int[] arr = {20, 30, 10, 5, 15};
        printGreater(arr);
    }

    private static void printGreater(int[] arr) {
        Deque<Integer> stack = new ArrayDeque<>();
        stack.push(arr[0]);
        System.out.println(-1); // previous greater for first element is always -1
        for (int i = 1; i < arr.length; i++) {
            while (!stack.isEmpty() && stack.peek() <= arr[i]) { // remove all the smaller previous element
                stack.pop();
            }
            System.out.println(stack.isEmpty() ? -1 : stack.peek()); // print the nearest previous greater element
            stack.push(arr[i]); // push the current element to process the next element and we dont need previous lesser elments;
        }
    }
}
