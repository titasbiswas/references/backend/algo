package ds.stack;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Span is the number of items on the left which are less than the current item, including the current one
 *
 * Span = current index - index of the nearest biggest item
 * Nearest biggest items can be tracked with those items index stack
 * {60, 10, 20, 15, 35, 50} => stack : print span(current-highest) => {0}:1, {0,1}:1, {0,2}:2, {0,2}:1, {0,2,3}:1, {0,4}:4, {0,5}:5
 */
public class StockSpan_2 {
    static void printSpan(int[] arr) {
        Deque<Integer> indexStck = new ArrayDeque<>();
        indexStck.push(0); //index of the first element
        System.out.println(1); //Span of the first element is always 1
        for (int i = 1; i < arr.length; i++) { // start from the second item
            while (!indexStck.isEmpty() && arr[indexStck.peek()] <= arr[i]){ // remove all the items in the stack which are <= the current item
                indexStck.pop();
            }
            int span = indexStck.isEmpty()?i+1:i - indexStck.peek(); //Only larger items present in stack; i+1 as it counts itself
            System.out.println(span);
            indexStck.push(i);
        }
    }

    public static void main(String[] args) {
        int arr[] = {60, 10, 20, 15, 35, 50, 20};
        printSpan(arr);
    }
}
