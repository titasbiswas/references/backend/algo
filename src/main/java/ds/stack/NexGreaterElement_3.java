package ds.stack;

import java.util.*;

public class NexGreaterElement_3 {
    public static void main(String[] args) {
        int[] arr = {5, 15, 10, 8, 6, 12, 9, 18};
        List<Integer> res = printGreater(arr);
        Collections.reverse(res);
        System.out.println(res);
    }

    private static List<Integer> printGreater(int[] arr) {
        Deque<Integer> stack = new ArrayDeque<>();
        List<Integer> res = new ArrayList<>();
        var len = arr.length;
        stack.push(arr[len-1]);
        res.add(-1); // next greater for last element is always -1
        for (int i = len-2; i >= 0; i--) {
            while (!stack.isEmpty() && stack.peek() <= arr[i]) { // remove all the smaller previous element
                stack.pop();
            }
            res.add(stack.isEmpty() ? -1 : stack.peek()); // print the nearest previous greater element
            stack.push(arr[i]); // push the current element to process the next element and we dont need previous lesser elments;
        }
        return res;
    }
}
