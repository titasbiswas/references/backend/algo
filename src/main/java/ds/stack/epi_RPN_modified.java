package ds.stack;

import java.util.Deque;
import java.util.LinkedList;

public class epi_RPN_modified {
    private static Integer eval(String s) {
        Deque<Integer> stack = new LinkedList<>();
        String[] symbols = s.split(",");
        if ("+-*/".contains(symbols[0])) {
            stack.addFirst(operate(symbols[0], Integer.parseInt(symbols[1]), Integer.parseInt(symbols[2])));
        }
        for (int i = 3; i < symbols.length; i++) {
            String symbol = symbols[i];
            if ("+-*/".contains(symbols[i])){
                int x = stack.removeFirst();
                int y = Integer.parseInt(symbols[i+1]);
                stack.addFirst(operate(symbols[i], x, y));
            }
        }
        return stack.removeFirst();
    }

    private static int operate(String symbol, int a, int b) {
        switch (symbol) {
            case "+":
                return a + b;
            case "-":
                return a - b;
            case "*":
                return a * b;
            case "/":
                return a / b;
            default:
                throw new IllegalArgumentException("Invalid operation");
        }
    }

    public static void main(String[] args) {
        String s= "+,3,4,*,2,+,1";//15
        System.out.println(eval(s));
    }
}
