package ds.stack;

import java.util.Stack;

public class CountParenthesisPair_0 {
    public static void main(String[] args) {
        String str ="()(()))))";// 1:{(}, 2:{} res =1, 3:{(} res =1, 4:{((} res =1, 4:{(} res =2,
        //5: {}, res = 3;
        String str1 = "((()";// 1:{(}, 2:{((}, 3:{(((}, 4: {((}, res=1
        String str3 = ")()())";//1:{}, 2:{(}, 3:{} res =1,
        System.out.println(solution(str)); //(((((((((( O(n)
    }

    private static int solution(String str){
        Stack<Character> stack = new Stack<>();
        int count = 0, res = 0;
        for(char c: str.toCharArray()){ // 0(n)
            if(c == ')'){
                if(!stack.isEmpty() && stack.peek() == '('){
                    res++;
                    stack.pop();
                }
            }
            else {
                stack.push(c);
            }
        }
        return 2*res;
    }
}
