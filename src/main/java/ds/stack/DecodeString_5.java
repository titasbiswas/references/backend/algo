package ds.stack;

import java.util.Stack;

public class DecodeString_5 {
    public static void main(String[] args) {
        String s = "3[a2[c]]2[bf]";//"3[a]2[bc]", "2[abc]3[cd]ef"
        System.out.println(solution(s));
    }

    private static String solution(String s) {
        Stack<Character> stack = new Stack<>();
        for (char c : s.toCharArray()) {
            if (c != ']') { // push everything except closing bracket
                stack.push(c);
            } else { // encounter closing bracket, processing time
                StringBuilder sb = new StringBuilder();
                while (!stack.isEmpty() && stack.peek() != '[') {// we pushed [ as well to demark number and char
                    sb.append(stack.pop()); //apending the chars, in reverse order
                }
                sb.reverse();// proper order;
                String tempStr = sb.toString();

                stack.pop();//removing the '['

                sb.setLength(0); // Emptying the sb

                while (!stack.isEmpty() && Character.isDigit(stack.peek())) {// we pushed [ as well to demark number and char
                    sb.append(stack.pop()); //apending the chars, in reverse order
                }
                sb.reverse();
                int count = Integer.valueOf(sb.toString());

                while(count > 0){ // Pushing all the processed chars back into the stack to process new chars
                    for(char ch : tempStr.toCharArray())
                        stack.push(ch);
                    count--;
                }

            }

        }
        StringBuilder resSb = new StringBuilder();
        while (!stack.isEmpty()){
            resSb.append(stack.pop());
        }
        resSb.reverse();
        return resSb.toString();
    }

}
