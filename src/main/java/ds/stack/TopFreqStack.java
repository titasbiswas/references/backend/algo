package ds.stack;

import java.util.*;


/**
 * Build a stack which pops item with the highest frequency
 * */
public class TopFreqStack {

    static Map<Integer, Integer> freqMap = new HashMap<>();
    static Map<Integer, Deque<Integer>> freqGroupMap = new HashMap<>();
    static int maxFreq = 0;

    // Function to insert x in the stack
    public static void push(int x)
    {
        int currFreq = freqMap.compute(x, (k, v) -> v==null?1:++v);
        if(currFreq>maxFreq)
            maxFreq = currFreq;
        freqGroupMap.computeIfAbsent(currFreq, v -> new ArrayDeque<>()).push(x);
    }

    // Function to remove maximum frequency element
    public static int pop()
    {
        int top = freqGroupMap.get(maxFreq).pop();
        freqMap.compute(top, (k,v)->--v);
        if(freqGroupMap.get(maxFreq).isEmpty())
            maxFreq--;
        return top;
    }

    // Driver code
    public static void main(String[] args)
    {

        // Push elements to the stack
        push(4);
        push(6);
        push(10);
        push(7);
        push(6);
        push(6);
        push(6);
        push(10);
        push(8);
        push(8);

        // Pop elements
        System.out.println(pop());
        System.out.println(pop());
        System.out.println(pop());
        System.out.println(pop());
        System.out.println(pop());
    }
}
