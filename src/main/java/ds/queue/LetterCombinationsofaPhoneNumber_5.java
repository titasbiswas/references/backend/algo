package ds.queue;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent. Return the answer in any order.
 *
 * * */
public class LetterCombinationsofaPhoneNumber_5 {

    public static void main(String[] args) {
        String digits = "29";
        List<String> res  = letterCombinations(digits);
        System.out.println(res);

    }
    public static List<String> letterCombinations(String digits) {
        if (digits == null || digits.length() == 0) {
            return new ArrayList<>();
        }
        String[] mappings = {"","","abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        Queue<String> q = new LinkedList<>();
        q.offer("");
        for(char digit : digits.toCharArray()){
            String letters = mappings[digit-'0'];
            int qSize = q.size();
            for (int i = 0; i < qSize; i++) {
                String prevLetter = q.poll();
                for(char currLetter: letters.toCharArray()){
                    q.offer(prevLetter+currLetter);
                }
            }
        }
        return new ArrayList<>(q);
    }

}
