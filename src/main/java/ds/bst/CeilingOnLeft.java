package ds.bst;

import java.util.Set;
import java.util.TreeSet;

public class CeilingOnLeft {


    public static void main(String[] args) {
        int[] arr = {2, 8, 30, 15, 25, 12};
        solution(arr);
    }

    private static void solution(int[] arr) {
        TreeSet<Integer> set = new TreeSet<>();
        set.add(arr[0]);
        System.out.println(-1);
        for (int i = 1; i < arr.length; i++) {
            if(set.ceiling(arr[i]) != null){
                System.out.println(set.ceiling(arr[i]));
            }
            else {
                System.out.println(-1);
            }
            set.add(arr[i]);
        }
    }

}

