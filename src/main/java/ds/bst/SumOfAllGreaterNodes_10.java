package ds.bst;

/**
 * Replace each node in binary search tree such that node value
 * is equal to sum of all the node values equal to or greater
 */

import lombok.ToString;
public class SumOfAllGreaterNodes_10 {
    public static void main(String[] args) {
        Node root = new Node(9);

        root.right = new Node(11);
        root.right.right = new Node(12);
        root.right.left = new Node(10);

        root.left = new Node(7);
        root.left.right = new Node(8);
        root.left.left = new Node(5);
        root.left.left.left = new Node(3);
        root.right.left.right = new Node(6);

        buildTree(root, 0);
        System.out.println(root);
    }

    private static int buildTree(Node root, int sum){
        if(root == null){
            return sum;
        }
        int sumRight = buildTree(root.right, sum) ;
        root.val = root.val + sumRight;
        return buildTree(root.left, root.val);
    }

    @ToString
    static class Node {
        int val;
        Node left, right;

        Node(int val){
            this.val = val;
        }
    }
}
