package ds.bst;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

public class BottomView {


    public static void topView(Node root) {
        Queue<Node> q = new LinkedList<>();
        Map<Integer, Integer> bvMap = new TreeMap<>();
        q.add(root);
        while (!q.isEmpty()) {
            Node node = q.poll();
            if (!bvMap.containsKey(node.hd)) {
                bvMap.put(node.hd, node.data);
            }
            if (node.left != null) {
                Node left = node.left;
                left.hd = node.hd - 1;
                q.add(left);
            }
            if (node.right != null) {
                Node right = node.right;
                right.hd = node.hd + 1;
                ;
                q.add(right);
            }
        }

        for (Map.Entry<Integer, Integer> e : bvMap.entrySet()) {
            System.out.print(e.getValue() + " ");
        }
    }

    public static void bottomView(Node root) {
        Queue<Node> q = new LinkedList<>();
        Map<Integer, Integer> bvMap = new TreeMap<>();
        q.add(root);
        while (!q.isEmpty()) {
            Node node = q.poll();
            bvMap.put(node.hd, node.data);
            if (node.left != null) {
                Node left = node.left;
                left.hd = node.hd - 1;
                q.add(left);
            }
            if (node.right != null) {
                Node right = node.right;
                right.hd = node.hd + 1;
                ;
                q.add(right);
            }
        }

        for (Map.Entry<Integer, Integer> e : bvMap.entrySet()) {
            System.out.print(e.getValue() + " ");
        }
    }

    public static void main(String args[]) {
        /*
                 10
               /    \
              20     30
             / \    /
            40  50 60
        *
        * */
        Node root = new Node(10);
        root.left = new Node(20);
        root.right = new Node(30);
        root.left.left = new Node(40);
        root.left.right = new Node(50);
        root.right.left = new Node(60);

        bottomView(root);
        System.out.println("");//40 20 60 30
        topView(root);
    }


    static class Node {
        int data, hd;
        Node left;
        Node right;

        public Node(int data) {
            this.data = data;
        }
    }
}
