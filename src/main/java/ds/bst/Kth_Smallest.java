package ds.bst;

public class Kth_Smallest {


    static AgNode effFindKthSmallest(AgNode root, int k){

        if(root == null )
            return null;

        int coeff = root.lCount+1;
        if(k == coeff){
            return root;
        }
        else if(k<coeff){
            return effFindKthSmallest(root.left, k);
        }
        else {
            return effFindKthSmallest(root.right, k-count);
        }
    }


    static int count = 0;
    static void findKthSmallest(Node root, int k){
        if(root != null){
            findKthSmallest(root.left,k);
            count++;
            if(count == k){
                System.out.println("Kth smallest is: "+root.val);
                return;
            }
            findKthSmallest(root.right,k);
        }
    }

    public static void main(String args[])
    {
        Node root=new Node(15);
        root.left=new Node(5);
        root.left.left=new Node(3);
        root.right=new Node(20);
        root.right.left=new Node(18);
        root.right.left.left=new Node(16);
        root.right.right=new Node(80);
        int k=3;

        System.out.print("Kth Smallest: ");
        findKthSmallest(root,k);


        AgNode agroot = null;
        int keys[] = { 20, 8, 22, 4, 12, 10, 14 };

        for (int x : keys)
            agroot = insertAgNode(agroot, x);

        int ak = 4;
        System.out.print("Kth Smallest: "+effFindKthSmallest(agroot, ak).data);
    }
    static AgNode insertAgNode(AgNode root, int data){

        if(root == null){
            return new AgNode(data);
        }
        if(data< root.data){
            root.left = insertAgNode(root.left, data);
            root.lCount++;
        }
        else if(data> root.data){
            root.right = insertAgNode(root.right, data);
        }
        return root;
    }
}

class Node{
    int val;
    Node left, right;

    public Node(int data){
        this.val =data;
    }
}

class AgNode{
    int data, lCount;
    AgNode left, right;

    public AgNode(int data){
        this.data=data;
    }
}
