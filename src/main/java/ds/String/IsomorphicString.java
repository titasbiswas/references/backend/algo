package ds.String;

import java.util.HashMap;
import java.util.Map;
/** Leetcode 205
 *  Given two strings s and t, determine if they are isomorphic.
 * Two strings s and t are isomorphic if the characters in s can be replaced to get t.
 * All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character, but a character may map to itself.
 * */
public class IsomorphicString {
    public static void main(String[] args) {

    }
    public boolean isIsomorphic(String s, String t) {
        Map<Character, Character> charMap = new HashMap<>();
        for(int i= 0; i< s.length(); i++){
            if(s.charAt(i) != t.charAt(i)){
                if(charMap.containsKey(s.charAt(i)) && charMap.get(s.charAt(i)) != t.charAt(i)){
                    return false;
                }else{
                    charMap.put(s.charAt(i),t.charAt(i));
                }
            }
        }
        return true;
    }
}
