package ds.String;

import java.util.ArrayList;
import java.util.List;

/**
 * Given an array of strings words and a width maxWidth, format the text such that each line has exactly maxWidth characters and is fully (left and right) justified.
 *
 * You should pack your words in a greedy approach; that is, pack as many words as you can in each line. Pad extra spaces ' ' when necessary so that each line has exactly maxWidth characters.
 *
 * Extra spaces between words should be distributed as evenly as possible. If the number of spaces on a line does not divide evenly between words, the empty slots on the left will be assigned more spaces than the slots on the right.
 *
 * For the last line of text, it should be left-justified, and no extra space is inserted between words.
 *
 * Note:
 *
 * A word is defined as a character sequence consisting of non-space characters only.
 * Each word's length is guaranteed to be greater than 0 and not exceed maxWidth.
 * The input array words contains at least one word.
 *
 *
 * Example 1:
 *
 * Input: words = ["This", "is", "an", "example", "of", "text", "justification."], maxWidth = 16
 * Output:
 * [
 *    "This    is    an",
 *    "example  of text",
 *    "justification.  "
 * ]
 * */
public class JustifyWords_10 {

    public static void main(String[] args) {
       String[] words = {"What","must","be","acknowledgment","shall","be"};
       int maxWidth = 16;
       fullJustify(words, maxWidth)
               .forEach(System.out::println);
    }
    public static List<String> fullJustify(String[] words, int maxWidth) {
        List<String> result=new ArrayList<>();
        int n = words.length;
        int i=0,j,lineLength;
        while(i<n){
            j=i+1;
            lineLength=words[i].length();
            while(j<n&&(lineLength+words[j].length()+(j-i-1)<maxWidth))
            {
                lineLength+=words[j].length();
                j++;
            }
            int diff =maxWidth-lineLength;
            int noOfWords=j-i;
            if(noOfWords==1||j>=n)
                result.add(leftJustify(diff,noOfWords,words,i,j));
            else
                result.add(middleJustify(diff,noOfWords,words,i,j));
            i=j;
        }
        return result;
    }
    public static String leftJustify(int diff,int noOfWords,String[] words,int i,int j)
    {
        StringBuilder  leftJustifyAns = new StringBuilder(words[i]);
        int spacesOnRight=diff-(j-i-1);
        for(int k=i+1;k<j;k++){
            leftJustifyAns.append(" "+words[k]);
        }
        leftJustifyAns.append(" ".repeat(spacesOnRight));
        return  leftJustifyAns.toString();
    }
    public static String middleJustify(int diff,int noOfWords,String[] words,int i,int j)
    {
        StringBuilder  middleJustifyAns = new StringBuilder(words[i]);

        int spacesNeeded= j-i-1;
        int spaces =diff/spacesNeeded;
        int extraSpaces = diff%spaces;
        for(int k=i+1;k<j;++k){
            int spacesToApply=spaces+(extraSpaces-- > 0 ? 1 : 0);
            middleJustifyAns.append(" ".repeat(spacesToApply)+words[k]);
        }
        return  middleJustifyAns.toString();
    }
}
