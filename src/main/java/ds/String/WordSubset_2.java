package ds.String;

import java.util.ArrayList;
import java.util.List;

/**
 * Leetcode 916
 * You are given two string arrays words1 and words2.
 * A string b is a subset of string a if every letter in b occurs in a including multiplicity.
 * For example, "wrr" is a subset of "warrior" but is not a subset of "world".
 *Input: words1 = ["amazon","apple","facebook","google","leetcode"], words2 = ["e","o"]
 * Output: ["facebook","google","leetcode"]
 * Input: words1 = ["amazon","apple","facebook","google","leetcode"], words2 = ["lo","eo"]
 * Output: ["google","leetcode"]
 *
 * Create a char count array of words 2 and each of words in words one and compare
 * */
public class WordSubset_2 {
    public static void main(String[] args) {
        String[] words1 = {"amazon","apple","facebook","google","leetcode"}, words2 = {"lo","eo"};
        List<String> res = wordSubsets(words1, words2);
        System.out.println(res);
    }
    public static List<String> wordSubsets(String[] words1, String[] words2) {
        List<String> res = new ArrayList<>();
        int[] subCharCount = new int[26];
        for(String word: words2){
            int[] tempCharFreq = createCharCount(word);
            for (int i = 0; i < 26; i++) {
                subCharCount[i] = Math.max(subCharCount[i], tempCharFreq[i]);
            }

        }

        for (String word: words1) {
            int[] charCount = createCharCount(word);
            if(isSubset(charCount, subCharCount)){
                res.add(word);
            }
        }
        return res;
    }

    private static boolean isSubset(int[] charCount, int[] words2Count) {
        for (int i = 0; i < 26; i++) {
            if(words2Count[i] > charCount[i]){
                return false;
            }
        }
        return true;
    }

    private static int[] createCharCount(String words) {
        int[] charCount = new int[26];
        for (int i = 0; i < words.length(); i++) {
            charCount[words.charAt(i)-'a']++;
        }
        return charCount;
    }
}
