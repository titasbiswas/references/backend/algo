package ds.hash;

import java.util.HashSet;

public class SubArrayWithZeroSum_2 {
    public static void main(String[] args) {
        int[] arr = {-3, 4, -3, -1, 1};
        System.out.println(solution(arr));
    }

    private static boolean solution(int[] arr) {
        HashSet<Integer> s = new HashSet<>();
        int ps = 0;
        for (int i = 0; i < arr.length; i++) {
            ps += arr[i];
            if (s.contains(ps))
                return true;
            if (ps == 0) // {-3, 2, 1, 4}
                return true;
            s.add(ps);
            System.out.println(ps);
        }
        return false;
    }
}
