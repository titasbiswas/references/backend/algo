package ds.hash;

import java.util.Map;
import java.util.TreeMap;

/**
 * Alice has some number of cards and she wants to rearrange the cards into groups so that each group is of size groupSize, and consists of groupSize consecutive cards.
 *
 * Given an integer array hand where hand[i] is the value written on the ith card and an integer groupSize, return true if she can rearrange the cards, or false otherwise.
 *
 *Example 1:
 *
 * Input: hand = [1,2,3,6,2,3,4,7,8], groupSize = 3
 * Output: true
 * Explanation: Alice's hand can be rearranged as [1,2,3],[2,3,4],[6,7,8]
 * */
public class HandOfStraight_5 {

    public static void main(String[] args) {
        int[] hand = {1,2,3,6,2,3,4,7,8};
        int groupSize = 3;
        System.out.println(isNStraightHand(hand, groupSize));
    }

    public static boolean isNStraightHand(int[] hand, int groupSize) {
        TreeMap<Integer,Integer> countMap = new TreeMap<>();
        for (int i : hand){
            countMap.merge(i, 1, Integer::sum);
        }
        //Iterate till i+groupSize for its existence
        while (countMap.size() > 0) {
            int curr = countMap.firstKey();
            for (int j = curr; j < curr+groupSize; j++) { // check whether we have all the numbers from cuu to curr+groupSize
                if(!countMap.containsKey(j)){
                    return false;
                }else {
                    countMap.merge(j, 0, (ov, nv) ->{
                        return ov > 1?--ov:null;
                    });
                }
                System.out.println(countMap);
            }
        }
        return true;
    }
}
