package ds.hash;

import java.util.HashSet;

/**
 * prefixsum_curr = prefixsum1+sum
 * => if (prefixsum_curr - sum) exists is set then the sum exits;
 * */
public class SubArrayWithGivenSum_3 {
    public static void main(String[] args) {
        int[] arr = {5, 8, 6, 13, 3, -1};
        int sum = 22;
        System.out.println(solution(arr, sum));
    }

    private static boolean solution(int[] arr, int sum) {
        HashSet<Integer> s = new HashSet<>();
        int ps = 0;
        for (int i = 0; i < arr.length; i++) {
            ps += arr[i];
            int diff = ps - sum;
            if(ps == sum)
                return true;
            if (s.contains(diff))
                return true;
            s.add(ps);
            System.out.println(ps);
        }
        return false;
    }
}
