package algo.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/** Leetcode 56
 * Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals, and return an array of the non-overlapping intervals that cover all the intervals in the input.
 * Soln: Sort the intervals by start time
 *  - Put the first interval in result list
 *  - Iterate through all the intervals starting from 1 and compare it with the last interval in the result list, if it overlaps, merge
 *  otherwise create a new interval
 *  */
public class MergeTimeIntervals_6 {
    public static void main(String[] args) {
       int[][] intervals = {{1,3},{2,6},{15,18}, {8,10}};
        System.out.println(Arrays.deepToString(merge(intervals)));
    }
    public static int[][] merge(int[][] intervals) {
        List<Interval> intervalList = new ArrayList<>();
        List<Interval> mergedLIntervalList = new ArrayList<>();
        for (int[] p : intervals) {
            intervalList.add(new Interval(p[0], p[1]));
        }
        Collections.sort(intervalList);
        mergedLIntervalList.add(intervalList.get(0)); // Add the first interval
        for (int i = 1; i < intervalList.size(); i++) {
            Interval lastInterval = mergedLIntervalList.get(mergedLIntervalList.size() - 1); // pop the last interval from the merged list
            Interval currInterval = intervalList.get(i);
            if (lastInterval.end >= currInterval.start) { //compare if the current interval overlaps with last merged interval
                int min = Math.min(lastInterval.start, currInterval.start);
                int max = Math.max(lastInterval.end, currInterval.end);
                lastInterval.start = min;
                lastInterval.end = max;
                //res.add(res.size() - 1, new Interval(min, max));
            } else {
                mergedLIntervalList.add(new Interval(currInterval.start, currInterval.end));
            }
        }

        mergedLIntervalList.forEach(e -> System.out.println(e.start + " " + e.end));


        int[][] resA = new int[mergedLIntervalList.size()][];
        int i = 0;
        for (Interval in : mergedLIntervalList) {
            resA[i] = new int[2];
            resA[i][0] = in.start;
            resA[i][1] = in.end;
            i++;
        }
        return resA;

    }

   static class Interval implements Comparable<Interval> {
        int start, end;

        public Interval(int start, int end) {
            this.end = end;
            this.start = start;
        }

        public int compareTo(Interval i) {
            return this.start - i.start;
        }
    }
}
