package algo.sorting;

import java.util.Arrays;

import static ds.array.ArrayUtils.swap;

public class DutchNationalFlag_5 {
    public static void main(String[] args) {
int[] arr = {2,0,2,1,1,0};
sortColors(arr);
Arrays.stream(arr).forEach(System.out::print);
    }
    public static void sortColors(int[] nums) {
        int l = 0, m = 0, h = nums.length-1;
        while(m<= h){
            if(nums[m] == 0){
                swap(nums, m, l);
                m++;
                l++;
            }
            else if(nums[m] == 1){
                m++;
            }
            else if(nums[m] == 2){
                swap(nums, m, h);
                h--;
            }
        }

    }
}
