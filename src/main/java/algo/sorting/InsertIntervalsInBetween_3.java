package algo.sorting;

import java.util.Arrays;

public class InsertIntervalsInBetween_3 {

    public static void main(String[] args) {
        int[][] intervals = {{1, 2}, {3, 5}, {6, 7}, {8, 10}, {12, 16}};
        int[] newInterval = {4, 8};
        insert(intervals, newInterval);
    }

    public static int[][] insert(int[][] intervals, int[] newInterval) {
        int[][] res;
        if(intervals.length == 0){
             res = new int[1][2];
             res[0] = newInterval;
             return res;
        }
        res = new int[intervals.length + 1][2];
        int[][] newIntervals = new int[intervals.length + 1][2];
        int i = 0;
        while (intervals[i][0] < newInterval[0] && i< intervals.length) {
            newIntervals[i] = intervals[i];
            i++;
        }
        newIntervals[i][0] = newInterval[0];
        newIntervals[i][1] = newInterval[1];
        i++;
        for (; i < newIntervals.length; i++) {
            newIntervals[i] = intervals[i - 1];
        }
        res[0] = newIntervals[0];
        int count = 0;
        for (int j = 1; j < newIntervals.length; j++) {
            if(newIntervals[j][0] <= res[count][1]){
                int start = Math.min(newIntervals[j][0],res[count][0]);
                int end = Math.max(newIntervals[j][1],res[count][1]);
                res[count][0]= start;
                res[count][1] = end;
            }
            else{
                count++;
                res[count] = newIntervals[j];
            }
        }

        res = Arrays.copyOf(res, count+1);
        System.out.println(Arrays.deepToString(res));

        return res;
    }
}
