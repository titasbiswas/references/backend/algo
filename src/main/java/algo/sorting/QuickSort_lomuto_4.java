package algo.sorting;

import java.util.Arrays;

import static misc.playground.tut.sorts.SelectionSort.swap;

public class QuickSort_lomuto_4 {
    public static void main(String[] args) {
        int[] arr = {8, 4, 7, 9, 3, 10, 5};
        sort(arr, 0, arr.length - 1);
        Arrays.stream(arr).forEach(System.out::print);
    }

    private static void sort(int[] arr, int l, int h) {
        if (l < h) {
            int p = lomutoPart(arr, l, h);
            sort(arr, l, p - 1);
            sort(arr, p + 1, h);
        }
    }

    private static int lomutoPart(int[] arr, int l, int h) {
        int p = arr[h];
        int i = l - 1;

        for (int j = l; j <= h - 1; j++) {
            if (arr[j] < p) {
                i++;
                swap(arr, i, j);
            }
        }
        swap(arr, i + 1, h);
        return i + 1;
    }
}
