package algo.concurrency;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

public class CheckAnagram {
    public static void main(String[] args) {

        BiFunction<String[], Integer, Integer[]> makeAnagram = (arr, i) -> {
            String a = arr[0];
            String b = arr[1];
            Map<Character, Integer> freqMap = new HashMap<>();
            int count = 0;
            for (int j = 0; j < a.length(); j++) {
                char ac = a.charAt(j);
                int freqA = freqMap.getOrDefault(ac, 0)+1;
                freqMap.put(ac, freqA);

                char bc = b.charAt(j);
                int freqB = freqMap.getOrDefault(bc, 0);
                freqMap.put(bc, --freqB);
            }

            for (Map.Entry<Character, Integer> e: freqMap.entrySet()) {
                count += Math.abs(e.getValue());
            }
            Integer[] res = new Integer[2];
            res[0] = i;
            res [1] = count/2;
            return res;
        };

        int itemCount = 0;
        List<Item> list = new ArrayList<>();
        list.add(new Item(new String[]{"abcc", "cccc"}, itemCount++));
        list.add(new Item(new String[]{"abc", "xyz"}, itemCount++));
        list.add(new Item(new String[]{"abc", "abc"}, itemCount++));
        int[] res = new int[list.size()];

        list.parallelStream()
                .map( item -> makeAnagram.apply(item.arr, item.index))
                .forEach(e ->{
                    res[e[0]] = e[1];
                });
        System.out.println(Arrays.toString(res));
    }
}

class Item{
    String[] arr;
    Integer index;

    public Item(String[] arr, Integer index){
        this.arr = arr;
        this.index = index;
    }
}
