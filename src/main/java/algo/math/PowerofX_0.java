package algo.math;

public class PowerofX_0 {
    public static void main(String[] args) {
        System.out.print(myPow(2.0, 3));
    }

    public static double myPow(double x, int n) {

        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return x;
        }

        double interPow = myPow(x, n / 2);
        if (n % 2 == 0) {
            return interPow * interPow;
        } else {
            return x * interPow * interPow;
        }

    }
}
