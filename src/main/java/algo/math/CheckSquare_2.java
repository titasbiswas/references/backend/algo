package algo.math;

public class CheckSquare_2 {
    // The below areEither of diagonal or a side
    static double line1 = 0;
    static double line2 = 0;
    public static boolean validSquare(int[] p1, int[] p2, int[] p3, int[] p4) {
        return ( decider(p1,p2) && decider(p1,p3) && decider(p1,p4) && decider(p2,p3) && decider(p2,p4) && decider(p3,p4));
    }

    private static boolean decider(int[] p1, int[] p2) {
        double dist = Math.sqrt(Math.pow(p1[0]-p2[0], 2)+Math.pow(p1[1]-p2[1], 2));
        if(dist == 0){
            return false;
        }
        else if(line1 == 0){ // either side or diagonal
            line1 = dist;
        }
        else if(line2 == 0 && line1 != dist){ // assign to the other one of side or diagonal
            line2 = dist;
        }
        else if (line1 != dist && line2 != dist) { //neither matches with diagonal or side
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        int p1[] = {1,0}, p2[] = {-1,0}, p3[] = {0,1}, p4[] = {0,-1};

        System.out.println(validSquare(p1, p2, p3, p4));
    }

}
