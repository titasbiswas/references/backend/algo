package algo.recursion;

public class AllPermutationOfString_2 {

    public static void main(String[] args) {
        printPermutations("ABC", 0);
    }

    private static void printPermutations(String s, int i){
        if(i == s.length()-1){
            System.out.println(s);
            return;
        }
        for (int j = i; j < s.length(); j++) {
            s = swapChar(s, i, j);
            printPermutations(s, i+1);
            s = swapChar(s, j, i);
        }
    }

    private static String swapChar(String s, int i, int j){
        char[] arr = s.toCharArray();
        char temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
        return new String(arr);
    }
}
