package algo.recursion;

import java.util.ArrayList;
import java.util.List;

/** Leetcode 22
 * BAcktracking
 * Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.
 * Input: n = 3
 * Output: ["((()))","(()())","(())()","()(())","()()()"]
 * Soln: Idea is to recursively explore all valid combinations.
 * There are 2 options in every step.
 * Open: if there are more paranthesis to open. if there is an open paranthesis, there must be a close paranthesis too. So increment closable by 1.
 * Cose: recursively do this iff there are any closable paranthesis left.
 * */
public class GenerateParenthesis_10 {
    private static List<String> res = new ArrayList<>();

    public static void main(String[] args) {
        int n = 3;
        System.out.println(generateParenthesis(n));
    }
    public static List<String> generateParenthesis(int n) {
        rec(n,0,"");
        return res;
    }

    private static void rec(int rem, int closable, String cur){

        if(rem==0 && closable==0){
            res.add(cur);
            return;
        }

        if(rem>0)
            rec(rem-1, closable+1, cur+"(");

        if(closable>0)
            rec(rem,closable-1,cur+")");
    }

}

