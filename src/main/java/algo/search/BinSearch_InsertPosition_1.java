package algo.search;

/**
 *
 * */
public class BinSearch_InsertPosition_1 {
    public static void main(String[] args) {
        int[] nums = {1,3,5,6};
        int target = 5;
        System.out.println(searchInsert(nums, target));
    }
    public static int searchInsert(int[] nums, int target) {
        int low = 0, high = nums.length - 1, n = nums.length;
        while(low <= high){
            int mid = low + (high-low)/2;
            if(nums[mid] == target){
                return mid;
            }else if (target > nums[mid]){
                if(mid == n-1 || target < nums[mid+1]){
                    return mid+1;
                }else{
                    low = mid+1;
                }
            }else if (target < nums[mid]){
                if(mid == 0 || target > nums[mid-1]){
                    return mid;
                }else{
                    high = mid-1;
                }
            }
        }
        return -1;
    }
}
