package algo.search;

public class ReapeatingInt_1 {
    public static void main(String[] args) {
        int[] arr = {0, 0};//{0, 2, 1, 3, 5, 4, 6, 5, 5};
        System.out.println(solution(arr));
    }
    private static int solution(int[] arr){
        int len = arr.length;
        int max = -1, sum = 0;
        for (int i = 0; i < len; i++) {
            sum += arr[i];
            max = Math.max(max, arr[i]);
        }
        int naturalSum = (max * (max + 1))/2;
        int lenDiff = len - max-1;
        int res = (sum - naturalSum)/lenDiff;
        return res;
    }
}
