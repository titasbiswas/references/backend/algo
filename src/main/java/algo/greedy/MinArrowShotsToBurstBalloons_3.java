package algo.greedy;

import java.util.Arrays;
/** Leetcode 452
 * There are some spherical balloons taped onto a flat wall that represents the XY-plane. The balloons are represented as a 2D integer array points where points[i] = [xstart, xend] denotes a balloon whose horizontal diameter stretches between xstart and xend. You do not know the exact y-coordinates of the balloons.
 * Arrows can be shot up directly vertically (in the positive y-direction) from different points along the x-axis. A balloon with xstart and xend is burst by an arrow shot at x if xstart <= x <= xend. There is no limit to the number of arrows that can be shot. A shot arrow keeps traveling up infinitely, bursting any balloons in its path.
 * Given the array points, return the minimum number of arrows that must be shot to burst all balloons.
 * Soln: Sort by end, as we can burst all the balloons ends in a range by shooting an arrow at the rightmost point
 *  - Sort by end
 *  - Add first one to brust with the first arrow
 *  - if next ones start also falls in the range i.e. start of the next balloon is less than the end of the previous range
 *  - else we have a new range a new arrow is required
 * */
public class MinArrowShotsToBurstBalloons_3 {

    public static int findMinArrowShots(int[][] points) {

        if(points.length == 0){
            return 0;
        }
        int res = 1;

        Arrays.sort(points, (x1, x2) ->{
            if(x1[1]==x2[1]) return 0;
            return x1[1]>x2[1]?1:-1;
        });//nlogn

        int currentEnd = points[0][1]; // set the current end oof the first ballon
        for(int i = 1; i<points.length;i++){
            if(points[i][0]<=currentEnd){ // if the start of this balloon is less than the end then it will be burst from the same arrow
                continue;
            }
            else { // we have a new range, and we need a new arrow
                res++;
                currentEnd = points[i][1];
            }
        }
        return res;

    }

    public static void main(String[] args) {
        int[][] blns = {{10,16},{2,8},{1,6},{7,12}};
        int[][] blns2 = {{1,2},{3,4},{5,6},{7,8}};
        int[][] blns3 = {{1,2},{2,3},{3,4},{4,5}};
        System.out.println(findMinArrowShots(blns3));
    }
}
