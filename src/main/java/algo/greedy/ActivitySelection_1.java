package algo.greedy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ActivitySelection_1 {
    private static List<Pair> selectMaxActivity(Pair[] activities) {
        Arrays.sort(activities);
        List<Pair> res = new ArrayList<>();
        res.add(activities[0]);
        for (int i = 1; i < activities.length; i++) {
            Pair lastPair = res.get(res.size() - 1);
            if (lastPair.end <= activities[i].start) {
                res.add(activities[i]);
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Pair[] actiities = {
                new Pair(12, 25),
                new Pair(10, 20),
                new Pair(20, 30),
        };
        selectMaxActivity(actiities).forEach(e -> System.out.println(e.start+" - "+e.end));
    }
}

class Pair implements Comparable<Pair> {
    int start, end;

    public Pair(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public int compareTo(Pair o) {
        if (this.end >= o.end)
            return 1;
        return -1;
    }
}
