package algo.greedy;

import java.util.Arrays;

public class FractionalKnapsack {
    private static double maxValue(Item[] items, double capacity) {
        Arrays.sort(items);
        double res = 0.0;
        double curr_cap = capacity;
        for (int i = 0; i < items.length; i++) {
            if (items[i].weight <= curr_cap) {
                res += items[i].value;
                curr_cap -= items[i].weight;
            } else {
                res += items[i].value * (curr_cap / items[i].weight);
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Item[] items = {
                new Item(50, 600),
                new Item(20, 500),
                new Item(30, 400),
        };
        System.out.println(maxValue(items, 70));
    }
}

class Item implements Comparable<Item> {
    double weight, value;

    public Item(double weight, double value) {
        this.weight = weight;
        this.value = value;
    }

    @Override
    public int compareTo(Item o) {
        if ((this.value / this.weight) <= (o.value / o.weight))
            return 1;
        return -1;
    }
}
