package algo.dp;

import java.util.Arrays;

public class LongestCommonSubsequence_1 {
    static int[][] memo;

    public static void main(String[] args) {
        String a = "AXYZ", b = "BAZ";

        System.out.println(lcsNaive(a, b, a.length(), b.length()));

        memo = new int[a.length() + 1][b.length() + 1];
        for (int[] row : memo) {
            Arrays.fill(row, -1);
        }

        System.out.println(lcsMemo(a, b, a.length(), b.length()));
    }

    static int lcsNaive(String a, String b, int i, int j) {

        if (i == 0 || j == 0) {
            return 0;
        }
        if (a.charAt(i - 1) == b.charAt(j - 1)) {
            return 1 + lcsNaive(a, b, i - 1, j - 1);
        } else {
            return Math.max(lcsNaive(a, b, i - 1, j), lcsNaive(a, b, i, j - 1));
        }
    }

    static int lcsMemo(String a, String b, int i, int j) {

        if (memo[i][j] != -1) {
            return memo[i][j];
        }
        if (i == 0 || j == 0) {
            memo[i][j] = 0;
        } else {
            if (a.charAt(i - 1) == b.charAt(j - 1)) {
                memo[i][j] = 1 + lcsMemo(a, b, i - 1, j - 1);
            } else {
                memo[i][j] = Math.max(lcsMemo(a, b, i - 1, j), lcsMemo(a, b, i, j - 1));
            }
        }

        return memo[i][j];
    }
}
